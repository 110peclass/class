#PYD802

#1. 題目說明:
#請開啟PYD802.py檔案，依下列題意進行作答，顯示字串每個字元對應的ASCII碼及其總和，
#使輸出值符合題意要求。作答完成請另存新檔為PYA802.py再進行評分。
#
#2. 設計說明：
#請撰寫一程式，要求使用者輸入一字串，顯示該字串每個字元的對應ASCII碼及其總和。
#
#3. 輸入輸出：
#輸入說明
#一個字串
#
#輸出說明
#依序輸出字串中每個字元對應的ASCII碼
#每個字元ASCII碼的總和
#
#輸入輸出範例
#範例輸入
#Kingdom
#範例輸出
#ASCII code for 'K' is 75
#ASCII code for 'i' is 105
#ASCII code for 'n' is 110
#ASCII code for 'g' is 103
#ASCII code for 'd' is 100
#ASCII code for 'o' is 111
#ASCII code for 'm' is 109
#713

total = 0 
string = input()

for i in range(0,len(string)):
    num = ord(string[i])        #ord(c)：C為字元參數，取得unicode碼10進位
    print("ASCII code for '%s' is %d" % (string[i],num))
    total += num
print(total)

###############################################################################

#PYD804

#1. 題目說明:
#請開啟PYD804.py檔案，依下列題意進行作答，將字串轉換成大寫及首字大寫，使輸出值符合
#題意要求。作答完成請另存新檔為PYA804.py再進行評分。
#
#2. 設計說明：
#請撰寫一程式，讓使用者輸入一字串，分別將該字串轉換成全部大寫以及每個字的第一個字母
#大寫。
#
#3. 輸入輸出：
#輸入說明
#一個字串
#
#輸出說明
#全部大寫
#每個字的第一個字母大寫
#
#輸入輸出範例
#範例輸入
#learning python is funny
#範例輸出
#LEARNING PYTHON IS FUNNY
#Learning Python Is Funny

st = input()
str1 = st.upper()
print(str1)
str2 = st.title()
print(str2)

###############################################################################

#PYD806

#1. 題目說明:
#請開啟PYD806.py檔案，依下列題意進行作答，計算指定字元出現的次數，使輸出值符合題意
#要求。作答完成請另存新檔為PYA806.py再進行評分。
#
#2. 設計說明：
#請撰寫一程式，讓使用者輸入一字串和一字元，並將此字串及字元作為參數傳遞給名為
#compute()的函式，此函式將回傳該字串中指定字元出現的次數，接著再輸出結果。
#
#3. 輸入輸出：
#輸入說明
#一個字串和一個字元
#
#輸出說明
#字串中指定字元出現的次數
#
#輸入輸出範例
#範例輸入
#Our country is beautiful
#u
#範例輸出
#u occurs 4 time(s)

def compute(sentence, w):
    return sentence.count(w)

sentence = input()
word = input()
print(word,'occurs',compute(sentence, word), 'time(s)')

###############################################################################

#PYD808

#1. 題目說明:
#請開啟PYD808.py檔案，依下列題意進行作答，進行社會安全碼格式檢查，使輸出值符合題意
#要求。作答完成請另存新檔為PYA808.py再進行評分。
#
#2. 設計說明：
#請撰寫一程式，提示使用者輸入一個社會安全碼SSN，格式為ddd-dd-dddd，d表示數字。若格
#式完全符合（正確的SSN）則顯示【Valid SSN】，否則顯示【Invalid SSN】。
#
#3. 輸入輸出：
#輸入說明
#一個字串（格式為ddd-dd-dddd，d表示數字）
#
#輸出說明
#判斷是否符合SSN格式
#
#輸入輸出範例
#範例輸入1
#329-48-4977
#範例輸出1
#Valid SSN
#範例輸入2
#837-a3-3000
#範例輸出2
#Invalid SSN

s = input()

isSSN = (len(s) == 11)              #安全碼共11位,判斷輸入的字串長度是否等於11
#                                    並將結果設定給變數isSSN(True/False)
if isSSN:                           #將isSSN設為條件式，True則繼續執行，False跳開
    for i in range(len(s)):         #若為True則以11位長度做為迴圈執行次數
        if i == 3 or i == 6:        #在字串位置的索引3及6
            if s[i] != '-':         #判斷是否 不等於'-'
                isSSN = False       #若是 不等於'-' 則這個字串給為False值
                break
        elif not s[i].isdigit():    #以反向判斷其他位置的索引值"不是"數值(digit)
            isSSN = False           #若是  不是數字  則這個字串給為False值
            break
    
if isSSN:                           #若為True 以上判斷都正常未被break
    print('Valid SSN')              #輸出Valid SSN
else:                               #其他狀況就
    print('Invalid SSN')

###############################################################################
    
#PYD810
    
#1. 題目說明:
#請開啟PYD810.py檔案，依下列題意進行作答，找出串列數字中最大值和最小值之間的差，使
#輸出值符合題意要求。作答完成請另存新檔為PYA810.py再進行評分。
#
#2. 設計說明：
#請撰寫一程式，首先要求使用者輸入正整數k（1 <= k <= 100），代表有k筆測試資料。每一
#筆測試資料是一串數字，每個數字之間以一空白區隔，請找出此串列數字中最大值和最小值之
#間的差。
#
#提示：差值輸出到小數點後第二位。
#
#3. 輸入輸出：
#輸入說明
#先輸入測試資料的筆數，再輸入每一筆測試資料（一串數字，每個數字之間以空白區隔）
#
#輸出說明
#每個串列數字中，最大值和最小值之間的差
#
#輸入輸出範例
#輸入與輸出會交雜如下，輸出的部份以粗體字表示
#4
#94 52.9 3.14 77 46
#90.86
#-2 0 1000.34 -14.4 89 50
#1014.74
#87.78 33333 29.3
#33303.70
#9998 9996 9999
#3.00
    
k = eval(input())

for i in range(k):              #以迴圈帶入K次執行次數
    num = input()               #輸入一筆"字串"資料
    num_lst = num.split(' ')    #將"字串"資料以空白鍵' '分割取出一個串列
    num_lst = [eval(x) for x in num_lst]    #以迴圈的方式把串列中的"字串"轉成數值
    print('%.2f' % (max(num_lst) - min(num_lst)))
    

#嘗試把字串改數值的迴圈 拆開來寫
k = eval(input()    )

for i in range(k):              #以迴圈帶入K次執行次數
    num = input()               #輸入一筆"字串"資料
    num_lst = num.split(' ')    #將"字串"資料以空白鍵' '分割取出一個串列

lst1=[]    
for x in num_lst:
    num = eval(x)
    lst1.append(num)
print('%.2f' % (max(lst1) - min(lst1)))
    