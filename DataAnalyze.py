#%%
#資料儲存的方法
import pandas as pd
datas = [[65,92,78,83,70],[90,72,76,93,56],[81,85,91,89,77],[79,53,47,94,80]]
indexs = ['李大年','王大同','黃美娟','陳美玲']
columns = ['國文','數學','英文','自然','社會']
df = pd.DataFrame(datas,columns=columns,index=indexs)
print(df)
df.to_csv(r'C:\Users\ASUS\Desktop\109100112\Python\DataAnalyze\pdout.csv',encoding='utf-8-sig')

#%%
#資料讀取的方法
import pandas as pd
rd = pd.read_csv(r'C:\Users\ASUS\Desktop\109100112\Python\DataAnalyze\pdout.csv' \
                 ,encoding = 'utf-8-sig',index_col=0)
print(rd)

#%%
#建立資料矩陣，numpy模組
import numpy as np
arr = np.array([[1,2,3],[4,5,6]])
#        ↑建立一個numpy陣列(矩陣)2*3矩陣
print(arr)
ran_arr = np.random.random((4,2))  #np.random.rand(4,2) 一樣
#            ↑建立隨機分布於0~1的4*2矩陣
print(ran_arr)

#%%
#視覺化繪圖
import matplotlib.pyplot as plt

listx = [1,5,7,9,13,16]
listy = [15,50,80,40,70,50]
plt.plot(listx,listy)
plt.show()

#%%
import matplotlib.pyplot as plt

listx1 = [1,5,7,9,13,16]
listy1 = [15,50,80,40,70,50]
plt.plot(listx1,listy1,label='Male')
listx2 = [2,6,8,11,14,16]
listy2 = [10,40,30,50,80,60]
plt.plot(listx2,listy2,color='red',linewidth=6,linestyle='--',label='Female')
#color = 色彩英文名稱or16進位值(色碼表)#00ff00 
#linewidth = 線寬 ；也可以縮寫成lw
#linestyle = 線的樣式 → -實線(預設) ； --虛線 ； -.點虛線 ； ..點線
plt.legend() #顯示圖例
plt.xlim(0,20)      #x軸的範圍(起始值,終止值)
plt.ylim(0,100)     #y軸的範圍(起始值,終止值)
plt.title('Pocket Money') #圖表的標題名稱
plt.xlabel('Age')         #x座標的標籤名稱
plt.ylabel('Money')       #y座標的標籤名稱
plt.show()

#%%
#運用numpy繪製曲線
import numpy as np
import matplotlib.pyplot as plt

x = np.arange(0,3*np.pi,0.1)
#np.arange = 迴圈的range
y = np.sin(x)
plt.title("sine wave form")
plt.plot(x,y)
plt.show()

#%%
#多圖一次呈現
import matplotlib.pyplot as plt

plt.figure()
#figure 繪圖畫面中可以再容納多個小圖形
plt.subplot(2,2,1)
#subplot(列數，欄數，位置)：將figure區域以欄列數劃分並將子圖繪製在位置處。排序由左至右，由上往下。
plt.plot([0,1],[0,1])

plt.subplot(2,2,2)
plt.plot([0,1],[0,2])

plt.subplot(2,2,3)
plt.plot([0,1],[0,3])

plt.subplot(2,2,4)
plt.plot([0,1],[0,5])

plt.show()

#%%直條圖(柱狀圖)
from matplotlib import pyplot as plt
x = [5,8,10]
y = [12,16,6]
x2 = [6,9,11]
y2 = [6,15,7]
plt.bar(x,y,align ='center')
#plt.bar(x座標串列,y座標串列,其他參數)
plt.bar(x2,y2,color = 'g',align = 'center')
#align = 'center' 直條的中央對齊座標刻度 ；align = 'edge' 直條的邊緣對齊座標刻度
plt.title('Bar graph')
plt.xlabel('X axis')
plt.ylabel('Y axis')
plt.show()

#%%在同個圖表中同時呈現兩組長條圖並列展示
import matplotlib.pyplot as plt
x = ['bk','sw','ph','rd','gu']
a = [8,7,1,6,5]
b = [12,2,9,5,3]
plt.bar(x,a,label='a',align = 'edge',width = 0.3)
plt.bar(x,b,label='b',align = 'edge',width=(-0.3))
#以對齊邊緣的方式，並設定width寬度，一組給正數、一組給負數，即可清楚展現兩組長條圖。

#%%
import matplotlib.pyplot as plt
import numpy as np

x = np.arange(1,11)
y = 2 * x + 5
plt.title('Matplotlib demo')
plt.xlabel('X axis caption')
plt.ylabel('Y axis caption')
#plt.plot(x,y)
plt.plot(x,y,'*b') 
# o → 實心圓點(s→實心方塊)(p→五邊形)(v→倒三角)(^→正三角)(*→五芒星形)(h/H→六邊形) ； b → blue藍色
plt.show()

#%%
#numpy.linspace(起始,終止,數量,其他參數)：產生間格數列 
import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(-1,1,50)
y = 2 * x + 1
y2 = x**2
plt.plot(x,y)
plt.show()
plt.plot(x,y2)
plt.show()

#%%
#圓餅圖：模組名稱.pie(資料串列,其他參數)   → pie預設為橢圓形
import matplotlib.pyplot as plt

labels = 'A','B','C','D','E','F'
size = [33,52,12,17,62,48]

plt.pie(size,labels=labels,autopct='%1.1f%%')
plt.axis('equal')

plt.show()

#%%

import matplotlib.pyplot as plt
font = {'family' : 'Microsoft JhengHei','weight' : 'bold','size'  : '12'}
plt.rc('font', **font) 
plt.rc('axes',unicode_minus=False) 

labels = ['東部','南部','北部','中部']
sizes = [5,10,20,15]
colors = ['r','g','b','y']
explode = (0,0,0.05,0)
plt.pie(sizes,explode=explode,labels=labels,colors=colors,\
        labeldistance=1.1,autopct="%.1f%%",shadow=True,\
        startangle=90,pctdistance=0.6)
plt.axis('equal')
plt.legend(loc="lower right")
plt.show()

#labeldistance : 資料標籤與圓心的距離是半徑的幾倍。
#shadow : 設定陰影
#startangle : 繪圖的起始角度(逆時針)
#pctdistance :　百分比文字與圓心的距離是半徑的幾倍
#explode : 設定圓形分離區塊的距離，以串列設定對應主資料的項目

#%%
#散布圖：scatter
import matplotlib.pyplot as plt
import numpy as np
import matplotlib

np.random.seed(20180731) #.random.seed設定隨機亂數種子(偽隨機→同個亂數種子產出的亂數一樣)
#arr = np.random.rand(4,2)
#print(arr)

x = np.arange(0.0,50.0,2.0)
y = x ** 1.3 + np.random.rand(*x.shape) * 30.0
#若x產生的值為浮點數，則以*x.shape轉為int
s = np.random.rand(*x.shape) * 800 +500

plt.scatter(x,y,s,c='g',alpha=0.5,marker=r'$\clubsuit$',label='Luck')
#x,y→數據；s→數量格式c→顏色；alpha→不透明度； marker→圖樣(club梅花)；label→標籤
plt.xlabel('Leprechauns')
plt.ylabel('Gold')
plt.legend(loc=2)
plt.show()

#%%
import matplotlib.pyplot as plt

x = [1,2,4,6,8,1,2,9,3]
y = [5,7,2,3,1,4,6,5,8]
#plt.scatter(x,y)
plt.scatter(x,y,s=[150 for i in range(len(x))],c='r',alpha=0.5)
plt.title('Scatter simple')
plt.xlabel('X axes')
plt.ylabel('Y axes')
plt.show()

#%%
import csv
from datetime import datetime
from matplotlib import pyplot as plt

# Get dates, high, and low temperatures from file.
filename = 'death_valley_2014.csv'
with open(filename) as f:
    reader = csv.reader(f)
    header_row = next(reader)

    dates, highs, lows = [], [], []
    for row in reader:
        try:
            current_date = datetime.strptime(row[0], "%Y-%m-%d")
            high = (int(row[1])-32)*5/9
            low = (int(row[3])-32)*5/9
        except ValueError:
            print(current_date, 'missing data')
        else:
            dates.append(current_date)
            highs.append(high)
            lows.append(low)

# Plot data.
fig = plt.figure(dpi=128, figsize=(5, 3))
plt.plot(dates, highs, c='red', alpha=0.5)
plt.plot(dates, lows, c='blue', alpha=0.5)
plt.fill_between(dates, highs, lows, facecolor='blue', alpha=0.1)

# Format plot.
title = "Daily high and low temperatures - 2014\nDeath valley, CA"
plt.title(title, fontsize=20)
plt.xlabel('', fontsize=16)
fig.autofmt_xdate()
plt.ylabel("Temperature (C)", fontsize=16)
plt.tick_params(axis='both', which='major', labelsize=16)
#plt.ylim(10, 120)

plt.show()
