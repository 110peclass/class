import jieba
import jieba.analyse
f = open(r'C:\Users\ASUS\Desktop\109100112\Python\TQC+\article.txt','r', \
         encoding='utf8')
article = f.read()
tags =  jieba.analyse.extract_tags(article, 10)
print('最重要的詞:',tags)

###############################################################################

import pdfkit
config = pdfkit.configuration( \
            wkhtmltopdf = r'C:\Program Files\wkhtmltopdf\bin\wkhtmltopdf.exe')

pdfkit.from_url("https://www.csf.org.tw/main/index.asp", \
                r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\out1.pdf', \
                configuration = config)

pdfkit.from_string('Hello World!', \
                   r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\out2.pdf', \
                   configuration = config)

pdfkit.from_file(r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\CSF.html', \
                 r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\out3.pdf', \
                 configuration = config)

pdfkit.from_file(r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\test.html', \
                 r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\out4.pdf', \
                 configuration = config)

from PyPDF2 import PdfFileReader, PdfFileWriter
read_file = r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\water.pdf'
PdfFileReader = PdfFileReader(read_file)

documentInfo = PdfFileReader.getDocumentInfo()  #取得頁面資訊
print('DocumentInfo = %s' % documentInfo)

pagelayout = PdfFileReader.getPageLayout()      #取得頁面配置
print('PageLayOut = %s' % pagelayout)

pagemode = PdfFileReader.getPageMode()          #取得頁面模式
print('PageMode = %s' % pagemode)

xmpMetadata = PdfFileReader.getXmpMetadata()    #取得檢所資料
print('xmpMetaData = %s' % xmpMetadata)

pagecount = PdfFileReader.getNumPages           #取得頁數
print('PageCount = %s' % pagecount)

###############################################################################

from PyPDF2 import PdfFileReader, PdfFileWriter
read_file = r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\health.pdf'
pdfFileReader = PdfFileReader(read_file,strict=False)

documentInfo = pdfFileReader.getDocumentInfo()  #取得頁面資訊
outFile = r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\health_output.pdf'
pdfFileWriter = PdfFileWriter()
numPage = pdfFileReader.getNumPages()           #取得頁數
    
for index in range(0,numPage):                  #使用迴圈執行總頁數的次數
    pageObj = pdfFileReader.getPage(index)      #取得頁面索引頁，把每一頁設為一個物件
    
    pdfFileWriter.addPage(pageObj)              #把每一個索引到的物件加入到output
    pdfFileWriter.write(open(outFile, 'wb'))    #寫入資料到檔案中(用二進位方式寫入)

pdfFileWriter.addBlankPage()                    #增加空白頁面
pdfFileWriter.write(open(outFile, 'wb'))        #把上述空白頁寫入檔案中

###############################################################################

from PyPDF2 import PdfFileReader, PdfFileWriter
readFile = r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\health.pdf' #取得檔案來源
pdfreader = PdfFileReader(readFile,strict = False) #讀取檔案並設定給pdfreader變數
documentinfo = pdfreader.getDocumentInfo()
outfile = r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\health_cut.pdf'
pdfwriter = PdfFileWriter()
numpages = pdfreader.getNumPages()
#print(numpages)
if numpages > 3:
    for index in range(3,numpages):
        pageobj = pdfreader.getPage(index)
        
        pdfwriter.addPage(pageobj)
      
    pdfwriter.write(open(outfile,'wb'))

###############################################################################
    
import PyPDF2
pdfFiles = [r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\out1.pdf', \
            r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\out2.pdf', \
            r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\out3.pdf']
pdfwriter = PyPDF2.PdfFileWriter()  #將檔案寫入設定成參數
pdfoutput = open( \
            r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\comb.pdf','wb')
            #以寫入模式開啟檔案
for filename in pdfFiles: #以檔案名稱做為迴圈的執行參數
    pdfreader = PyPDF2.PdfFileReader(open(filename,'rb'))
    #以讀取模式開啟，並設定給pdfreader變數(3個pdf物件)
    for pagenum in range(pdfreader.numPages):#把檔中的總頁數作為迴圈參數
        pdfwriter.addPage(pdfreader.getPage(pagenum))
#       ↑將讀取的頁面加入    ↑將檔案中的每一頁一個一個取出(pdf頁面物件)  
pdfwriter.write(pdfoutput)
pdfoutput.close()

###############################################################################

f = open(r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\score.txt')
a = f.read()
L = a.split()
for i in range(0,len(L)):
    L[i] = int(L[i])
c = [0,0,0,0,0,0]
for x in L:
    if x >= 90:
        c[0] += 1
    elif x >= 80:
        c[1] += 1
    elif x >= 70:
        c[2] += 1
    elif x >= 60:
        c[3] += 1
    elif x >= 40:
        c[4] += 1
    else:
        c[5] += 1
        
print('90分以上%d人' % c[0],end = '\n')
print('80-89分%d人' % c[1],end = '\n')
print('70-79分%d人' % c[2],end = '\n')
print('60-69分%d人' % c[3],end = '\n')
print('40-59分%d人' % c[4],end = '\n')
print('39分以下%d人' % c[5],end = '\n')

###############################################################################

import math
with open( \
    r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\data5.txt','r') as fin:
    with open( \
    r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\data5_w.txt','w') as fout:
              for line in fin: #將資料逐筆讀取
                  data = math.ceil(20/(float(line)*0.001425))
                  #將資料類型定為浮點數進行乘除後，以math.ceil取出大於等於的整數
                  print('每股價格:%5.2f,每日需購股數:%5.0f' % (float(line),data))
                  fout.write(str(data)+'\n')

###############################################################################

import csv
with open(r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\ubike_1.csv', \
          'r',encoding = 'utf8') as csvfile:
    plots = csv.reader(csvfile, delimiter= ',')
    for row in plots:
        print(row[0]+' '+row[1]+' '+row[3]+' '+row[5]+' '+row[12])

###############################################################################
        
import csv
with open(r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\air.csv', \
    'r',encoding = 'utf8') as csvfile:
    plots = csv.reader(csvfile,delimiter = ',')
    for row in plots:
        print(row[0]+' '+row[2]+' '+row[3])

###############################################################################
        
import csv
with open(r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\stock.csv', \
          'r') as fin:
    with open(r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\stock_w.csv', \
              'w') as fout:
        csvreader = csv.reader(fin,delimiter = ',')
        csvwriter = csv.writer(fout,delimiter = ',')
        header = next(csvreader)   #將每一條資料逐行讀出，並設定給變數header
        print(header)
        csvwriter.writerow(header) #將header逐行寫入
        for row in csvreader:      #以迴圈方式讀取csvreader
            row[6] = row[6].replace('／','-') #將日期中的／替代為-
            print(','.join(row))   #將每個row前面加上','使成為csv','分隔模式
            csvwriter.writerow(row) #將資料寫入
            
###############################################################################
            
import json
print(json.dumps(['two',{'bar':('jaz',None,2.0,1)}]))
print(json.dumps("\"two\bar"))
print(json.dumps('\u4321'))
print('\\')
print(json.dumps({'c':0,'b':0,'d':0}, sort_keys=True))
print(json.dumps([0,1,2,3,{'4':5,'6':7}], separators = (',',':')))
print(json.dumps({'4':5,'6':7}, sort_keys = True, indent=1))
d1 = {'b':789,'c':456,'a':123}
d2 = json.dumps(d1,sort_keys = True, indent = 4)
print(d2)

#JSON型態轉換到Python型態的對照：
#object      →dict
#array       →list
#string      →unicode
#number(int) →int,long
#number(real)→float
#true        →True
#false       →False
#null        →None

#json.dumps  >>> 把str轉成dict並寫入json檔
#json.dump   >>> 把dict轉成str並寫入json檔
#json.load   >>> 從json檔中讀取資料
#json.loads  >>> 將str資料轉成dict

                
###############################################################################

import json
with open(r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\ubike_1.json', \
          'r',encoding = 'utf8') as file:  #開啟檔案
    data = json.load(file)                 #將json檔案轉為python可讀並指定給變數
    for item in data:                      #以迴圈方式
        print([item['sno'],item['sna'],item['tot']])
          
###############################################################################

import xml.etree.ElementTree as et #載入套件
tree = et.ElementTree(file = \
        r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\menu.xml')
        #以元素樹狀結構開啟檔案
root = tree.getroot() #取得根結點 >>> menu
print(root.tag)
for child in root:    #取得root下一層的根結點 >>>breakfast,lunch,dinner
    print('tag:',child.tag,'attributes:',child.attrib) 
#                ↑取得結點標籤(名稱)      ↑取得結點屬性
    for grandchild in child: #取得child下一層的根結點 >>>item
        print('\ttag',grandchild.tag,'attributes:',grandchild.attrib)
print(len(root))        #root下的節點長度(個數) >>> 菜單選項的數目(三餐)
print(len(root[0]))     #擷取root下第一個節點的長度(個數) >>> 兩個價位

#XML：eXtensible Markup Language；可延伸標記語言
#→是一種電腦標記語言(標籤語法)
#→規則特性：
#    →是一種標籤語法
#    →以<名稱>開頭，後面接一段內容，再以</名稱>結尾。
#    →忽略空格
#    →<名稱>下可以有<子名稱>，層層結構。
#    →<名稱>可稱為一個節點
#    →<名稱 屬性=屬性值>：代表該名稱的設定功能
#    →通常用於資料傳遞與消息發佈，如RSS....，一般業界會自訂客製化的XML格式。           
                
###############################################################################                
                
import xml.etree.ElementTree as et
tree = et.ElementTree(file = \
        r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\country_data.xml')
root = tree.getroot()                
print('Country_data.xml的根節點:'+root.tag)
print('根節點標籤裡的屬性和屬性值:'+str(root.attrib))

for child in root:
    print(child.tag,child.attrib)
print('排名:'+root[0][0].text,'國內生產總值:'+root[0][2].text)

for neighbor in root.iter('neighbor'):
    #iter>>>尋找節點下所有元素內符合目標值的項目
    print(neighbor.attrib)
#    print(neighbor.tag)
    
for country in root.findall('country'):
    #在root下找出所有的標籤為country的節點
    rank = country.find('rank').text
    #以find方法搜尋country下的rank，並取出其文字，
    name = country.get('name')
    #以get方法取出country下的name的屬性
    print(name,rank)
    
###############################################################################

import xml.etree.ElementTree as et
tree = et.parse(r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\country_data.xml')
root = tree.getroot()

for rank in root.iter('rank'):  #以迴圈搜尋root下符合rank的項目
    new_rank = int(rank.text)+1 #將rank的標籤文字轉為整數型態後+1
    rank.text = str(new_rank)   #把rank標籤文字設定成new_rank並轉為字串
    rank.set('updated','yes')   #以set方法把rank的屬性及屬性值設定進去

tree.write(r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\country_data.xml', \
    encoding = 'utf8')

#%%
# XML檔的寫入
import xml.etree.ElementTree as ET #載入 xml.etree.ElementTree 套件
tree = ET.parse(r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\country_data.xml') #以parse讀取解析XML檔案
root = tree.getroot() #取得根節點<data>

for rank in root.iter("rank"): #以迴圈取得<root>節點下符合rank的節點，執行修改 rank 標籤    
    new_rank=int(rank.text)+1 #設定 rank 標籤顯示的文字轉換為整數後 + 1，並設定給變數new_rank        
    rank.text=str(new_rank)#將加 1 後的顯示文字轉換為字串，並設定給變數rank.text
    
    #Element.set(屬性 , 屬性值)：設定元素的屬性、屬性值
    rank.set("updated","yes")#以set()方法設定 rank 標籤的屬性、屬性值 標註以修改過

#利用write()方法寫入 XML 資料
tree.write(r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\country_data.xml',
           encoding="utf-8")

###############################################################################

import xml.etree.ElementTree as ET
tree = ET.parse(r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\country_data.xml')
root = tree.getroot()
for country in root.findall('country'):
    rank = int(country.find('rank').text)
    if rank > 50:
        root.remove(country)
tree.write(r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\country_data.xml')

#%%

#XML檔的刪除
#載入 xml.etree.ElementTree 套件
import xml.etree.ElementTree as ET

#以parse讀取解析XML檔案
tree = ET.parse(r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\country_data.xml')
#取得根節點<data>
root = tree.getroot()

#以 findall() 方法取得根節點下的子節點標籤中符合country的標籤取出
#再以迴圈執行根節點下所有 country 標籤
for country in root.findall('country'):

#以find()方法尋找 country 標籤下的子節點 rank 標籤的文字並轉換為整數
    rank=int(country.find("rank").text)
    
    if rank>50:     #若 rank 標籤顯示文字大於 50
         #則使用 remove() 方法移除根節點下的 country 標籤
        root.remove(country)

#利用write()方法寫入 XML 資料
tree.write(r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\country_data.xml', encoding="utf-8")

###############################################################################

import sqlite3

conn = sqlite3.connect(r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\test.db')
print('Opened database successfully')

c = conn.cursor()

c.execute('''CREATE TABLE COMPANY1
          (ID INT PRIMARY KEY NOT NULL,
          NAME TEXT NOT NULL,
          AGE INT NOT NULL,
          ADDRESS CHAR(50),
          SALARY REAL);''')
print('Table created sucessfully')

conn.commit()
conn.close()

#    →sqlite3.connect：開啟資料庫的連結，成功開啟則傳回一個連線物件。
#    →sqlite3.cursor：建立cursor(資料指標)
#    →sqlite3.execute：執行SQL語法
#    →sqlite3.commit：提交目前的交易(執行資料庫的操作)
#    →sqlite3.rollback：回復上一次呼叫commit()對資料庫的更改。
#    →sqlite3.close：關閉資料庫連結
###############################################################################

import sqlite3

conn = sqlite3.connect(r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\test.db')
c = conn.cursor()
print('Opened database successfully')

c.execute("INSERT INTO COMPANY1 (ID,NAME,AGE,ADDRESS,SALARY) \
          VALUES(1,'Paul',32,'California',20000.00), \
          (2,'Allen',25,'Texas',15000.00), \
          (3,'Teddy',23,'Norway',20000.00), \
          (4,'Mark',25,'Rich-Mond',65000.00)");
conn.commit()
print('Records created sucessfully')
conn.close()
          
###############################################################################

import sqlite3
conn = sqlite3.connect(r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\test.db')
c = conn.cursor()

cursor = c.execute("SELECT * FROM COMPANY1") #注意select all 是5個column
for row in cursor:                           #迴圈所索引的是4個column而以
    print('ID =',row[0])
    print('NAME=',row[1])
    print('ADDRESS = ',row[3])
    print('SALARY = ',row[4], '\n')
conn.close()

###############################################################################

import sqlite3
conn = sqlite3.connect(r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\test.db')
c = conn.cursor()

c.execute("UPDATE COMPANY1 SET SALARY = 25000.00 WHERE ID=1")
conn.commit()
print('Total number of rows updated :',conn.total_changes)
cursor = conn.execute("SELECT * FROM COMPANY1")
for row in cursor:
    print('ID = ',row[0])
    print('NAME=',row[1])
    print('ADDRESS = ',row[3])
    print('SALARY = ',row[4], '\n')
conn.close()

###############################################################################

import sqlite3
conn = sqlite3.connect(r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\test.db')
c = conn.cursor()

c.execute("DELETE FROM COMPANY1 WHERE ID = 2;")
conn.commit()
print('Total number of rows deleted :', conn.total_changes)
cursor = conn.execute('SELECT * FROM COMPANY1')
for row in cursor:
    print('ID = ',row[0])
    print('NAME=',row[1])
    print('ADDRESS = ',row[3])
    print('SALARY = ',row[4], '\n')
conn.close()

###############################################################################

import csv
import requests
from bs4 import BeautifulSoup
from time import localtime,strftime,strptime,mktime
from datetime import datetime
from os.path import exists

html = requests.get("https://rate.bot.com.tw/xrt?Lang=zh-TW") #取得網站內容

bsobj = BeautifulSoup(html.content,'lxml') 
#透過BeautifulSoup分析這個html的內容，成為樹狀結構的物件

for single_tr in bsobj.find('table',{'title':'牌告匯率'}).find('tbody').findAll('tr'):
#                           ↑找表格   ↑屬性是title:牌告匯率      ↑再找下一層的tbody  ↑找所有的tr(列)
    
    cell = single_tr.findAll('td') 
#   找出所有的td(欄)
    currency_name = cell[0].find('div',{'class':'visible-phone'}).contents[0]
#                   ↑在索引的第一個欄中找到div  ↑屬性是class:visible-puhone  
    currency_name = currency_name.replace('\r','') #去除歸位
    currency_name = currency_name.replace('\n','') #去除換行
    currency_name = currency_name.replace(' ','')  #去除空白
    currency_rate = cell[2].contents[0]
#                   ↑在索引的第三個欄中找到內容的第一筆資料
    print(currency_name,currency_rate)
    file_name = r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\bkt_rate' \
    + currency_name + '.csv'
    now_time = strftime('%y-%m-%d %H:%M:%S',localtime())
    if not exists(file_name):
        data = [['時間','匯率'],[now_time,currency_rate]]
    else:
        data = [[now_time, currency_rate]]
    f = open(file_name,'a')
    w = csv.writer(f)
    w.writerows(data)
    f.close()    
    
###############################################################################

#exercise
#在環保署的細懸浮微粒資訊中，用python撰寫程式讀取資料。(json,html,csv)
#csv
import csv
with open(r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\pm2.5_csv.csv', \
          'r',encoding = 'utf8') as csvfile:
    sort = csv.reader(csvfile,delimiter = ',')
    for row in sort:
        print(row)

#json
import json
with open(r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\pm2.5_json.json', \
          'r',encoding = 'utf8') as jsonfile:
    sort = json.load(jsonfile)
    for row in sort:
        print(row)
          
#xml
import xml.etree.ElementTree as et
tree = et.parse(r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\pm2.5_xml.xml')
 
root = tree.getroot()

for branch in root.findall('Data'):
    site = branch.find('Site').text
    county = branch.find('county').text
    pm = branch.find('PM25').text
    print('測站:',site,'--縣市',county,'--PM2.5',pm)
    
###############################################################################
    
from __future__ import unicode_literals, print_function
#__future__模組 → 確保在新舊的版本中可以相容使用功能
import urllib  #下載網頁
from bs4 import BeautifulSoup
#BeautifulSoup 的網頁解析器 → html.parser , lxml , xml , html5lib
import urllib.request

requests_url = 'http://invoice.etax.nat.gov.tw/'

htmlcontent = urllib.request.urlopen(requests_url).read()
#開起網頁連結，並以read()讀取網頁內容
soup = BeautifulSoup(htmlcontent,"html.parser")
#建立物件  (從網頁內容中取得)   (並以html.parser解析)
results = soup.find_all('span',class_ = 't18Red')  #有8個
subTitle = ['特別獎','特獎','頭獎','增開六獎']
months = soup.find_all('h2',{'id':'tabTitle'})  #有2個

month_newest = months[0].find_next_sibling('h2').text
#                           ↑在同一層往後尋找'h2'  ↑轉成text類型
month_previous = months[1].find_next_sibling('h2').text
print('最新一期統一發票開獎號碼({0}):'.format(month_newest))
for index, item in enumerate(results[:4]): 
#                  ↑以列舉方式找出results前四個結果(索引值,results)
    print('>> {0} : {1}'.format(subTitle[index], item.text))
#             ↑輸出格式化       ↑取得索引的subTitle  ↑把results轉成text類型
print('上期統一發票開獎號碼({0}):'.format(month_previous))
for index2, item2 in enumerate(results[4:8]):
    print('>> {0} : {1}'.format(subTitle[index2], item2.text))
    
###############################################################################
#列舉法
a1 = [1,2,3,4,5,6]
for index in enumerate(a1[:3]):
    print((index))
   
###############################################################################
    
import requests
url = 'https://tw.yahoo.com/'
html = requests.get(url)
html.encoding = 'utf-8'
if html.status_code == requests.codes.ok:
    print(html.text)
    
###############################################################################

import requests
payload = {'key1':'value1','key2':'value2'} 
html = requests.get('http://httpbin.org/get', params = payload)
print(html.url)
#http://httpbin.org/get若帶有參數的請求，則以?&合併於網址後

import requests
payload = {'key1':'value1','key2':'value2'} 
html = requests.post('http://httpbin.org/post', data = payload)
print(html.text)
print(html.url)

###############################################################################

import requests
from bs4 import BeautifulSoup
payload = {
        'from':'https://www.ptt.cc/bbs/Gossiping/index.html',
        'yes':'yes'
        }
headers = {
        'user-agent':'Mozilla/5.0 (Macintosh;Intel Mac OS X 10_12_3)' \
        'AppleWebkik/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'
        }  #讓程式模擬瀏覽器騙過網頁伺服器的登入

rs = requests.session() #建立身分認證物件(要求session)

rs.post('https://www.ptt.cc/ask/over18', data = payload, headers = headers)
#把建立的session以post的方式帶入參數，伺服器端就會產生所屬的cookies登入

res = rs.get('https://www.ptt.cc/bbs/Gossiping/index.html',headers = headers)

soup = BeautifulSoup(res.text,'html.parser')
items = soup.select('.r-ent')
for item in items:
    print(item.select('.date')[0].text, item.select('.author')[0].text, \
          item.select('.title')[0].text)
    
###############################################################################   
    
html = """
<html><head><title>網頁標題</title></head>
<p class = 'header'><h2>文件標題</h2></p>
<div class = 'content'>
    <div class = 'item1'>
        <a href='http://example.com/one' class = 'red' id = 'link1'>First</a>
        <a href='http://example.com/two' class = 'red' id = 'link2'>Second</a> 
    </div>
    <a href='http://example.com/three' class = 'blue' id = 'link3'>
        <img src='http://example.com/three.jpg'>Third
    </a>
</div>
</html>"""

from bs4 import BeautifulSoup
sp =  BeautifulSoup(html,'html.parser') #網頁程式碼的內容
#print(sp.title)                     #取得sp網頁標題的「內容」 → 「網頁標題」
#print(sp.find('h2'))                #取得所有標題為h2的「內容」 → 「文件標題」
#print(sp.find_all('a'))             #取得所有標籤為a的「內容」，3筆
#print(sp.find_all("a", {"class":"red"})) #取得所有標題為a且屬性class=red 的「內容」，2筆
#data1 = sp.find("a",{"href":"http://example.com/one"}) #取得標籤a且屬性符合的「內容」，類型為 → 字串!!
#print(data1.text)
#data2 = sp.select("#link1")          #取得屬性id=link1的「內容」,類型為 → 串列!!，1筆      
##print(data2[0].text)                 #取得第一筆資料的內容並轉為text類型
##print(data2[0].get("href"))          #第一筆資料的內容
#print(data2[0]['href'])             #搜尋第一筆資料中屬性為href的值，類型為 → 字串!
#print(sp.find_all(['title','h2']))   #取得所有資料中標題為'title'以及'h2'的內容
#print(sp.select('div img')[0]['src'])#搜尋div 下的img 的第一筆資料內容當中屬性為src的值

###############################################################################

from selenium import webdriver

driver_path = r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\chromedriver.exe'
url =  'https://github.com/login'
email = ''
password = ''
driver = webdriver.Chrome(driver_path)

driver.maximize_window()   #把視窗最大化
driver.get(url)

driver.find_element_by_id('login_field').send_keys(email)    #找到username這個標籤
driver.find_element_by_id('password').send_keys(password)
driver.find_element_by_id('commit').click()

###############################################################################

from selenium import webdriver
import time

driver_path = r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\chromedriver.exe'
web = webdriver.Chrome(driver_path)
web.get('https://www.cwb.gov.tw/V7/')
web.set_window_position(0,0)    #設定視窗位置
web.set_window_size(700,700)    #設定視窗大小，單位是像素
time.sleep(5)                   #讓執行暫停5秒
web.find_element_by_link_text('衛星').click() #取得符合"衛星"的元素連結，並點一下
time.sleep(5)
web.close()

###############################################################################

from selenium import webdriver
url = 'https://tw.yahoo.com/'
driver_path = r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\chromedriver.exe'
browser = webdriver.Chrome(driver_path)
browser.get(url)

element = browser.find_element_by_id('UHSearchBox')
element.send_keys('Hello World')
submit = browser.find_element_by_id('UHSearchWeb').click()

###############################################################################

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
driver_path = r'C:\Users\ASUS\Desktop\109100112\Python\WebScript\chromedriver.exe'
driver = webdriver.Chrome(driver_path)
driver.get('http://www.python.org')
print(driver.title)
assert 'Python' in driver.title         #assert+條件 則執行下述程式碼
elem = driver.find_element_by_name('q')
elem.clear()
elem.send_keys('pycon')
elem.send_keys(Keys.RETURN)
assert 'No results found.' not in driver.page_source
driver.close()

###############################################################################
#%%
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
driver_path = r'D:\Users\Vanity\Desktop\Python\WebScript\chromedriver.exe'
driver = webdriver.Chrome(driver_path)
driver.get('http://www.imdb.com')
search_elem = driver.find_element_by_css_selector('#suggestion-search')          #搜尋ID的屬性(網頁的搜尋欄)
search_elem.send_keys('The Shape of Water')                                     #送出鍵入值
time.sleep(3)
search_button_elem = driver.find_element_by_css_selector('#suggestion-search-button')#搜尋ID的屬性(網頁搜尋欄的搜尋動作按鈕)
search_button_elem.click()
time.sleep(3)
first_result_elem = driver.find_element_by_css_selector('#main .findSection .odd:nth-child(1) .result_text a')
#                                                       ↑逐層搜尋ID屬性 "以及" ↑class屬性 之下的odd(單數函式)的子項目第1筆 的下一層的class屬性是reesult_text   的再下一層 a的屬性 =href(超連結)             
first_result_elem.click()
time.sleep(3)
rating_elem = driver.find_element_by_css_selector('strong span')
#逐層搜尋標籤名稱為strong 之下的 sapn (找到網頁評分7.3分)
rating = float(rating_elem.text)
#cast_elem = driver.find_element_by_css_selector('.itemprop .itemprop')  #逐層搜尋ID為itemprop 之下的itemprop (找到網頁評分的基數10分)
cast_elem = driver.find_elements_by_css_selector('#main_bottom .article .cast_list tbody')  #逐層找到ID為main_bottom 之下的class
cast_list = [cast.text for cast in cast_elem]                           #上面那一行我想老師應該是寫錯了，itemprop下的所有標題沒有cast的內容

driver.close()
print(rating, cast_list)
