import pandas as pd
from os import walk
from os.path import join

mypath = r"C:\Users\ASUS\Desktop\109100112\Python\Data_exercise\report\Movie"  
for root, dirs, files in walk(mypath):      
  for f in files:  
    fullpath = join(root,f)         
    globals()['df'+f.replace('.csv','')] = pd.read_csv(fullpath)  
    globals()['df'+f.replace('.csv','')] = (
            globals()['df'+f.replace('.csv','')]
            .drop(['序號','申請人','累計銷售金額','累計銷售票數']
            ,axis=1)
            )
    globals()['df' + f.replace('.csv', '')].loc[:, '銷售票數'] = (
            globals()['df' + f.replace('.csv', '')]
            .loc[:,'銷售票數']
            .apply(lambda x: ''.join([i for i in x if i.isdigit()]))
            .astype(int)
            )
    globals()['df' + f.replace('.csv', '')].loc[:, '周票數變動率'] = (
            globals()['df' + f.replace('.csv', '')]
            .loc[:,'周票數變動率']
            .str.replace('%','')
            .str.replace(',','')
            .astype(float)
            .apply(lambda x: x/100)
            )
    globals()['df' + f.replace('.csv', '')].loc[:, '銷售金額'] = (
            globals()['df' + f.replace('.csv', '')]
            .loc[:,'銷售金額']
            .apply(lambda x: ''.join([i for i in x if i.isdigit()]))
            .astype(int)
            )

