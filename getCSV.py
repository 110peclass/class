import requests
from bs4 import BeautifulSoup

html = requests.get('https://data.gov.tw/dataset/94224')
bsobj = BeautifulSoup(html.content,'lxml')

for single_div in bsobj.find('div',{'class':'field field-name-field-dataset-resource field-type-dgresource-resouce field-label-inline clearfix'}).find('div',{'class','field-items'}).findAll('a',string='CSV'):
    
    href = single_div.get('href')
    currency_name = single_div.parent.find('span',{'class','ff-desc'}).text
    currency_name = re.sub('[全國電影票房.]','',currency_name)
  
    file_name = r'C:\Users\ASUS\Desktop\109100112\Python\Data_exercise\report\Movie\m' + currency_name + '.csv'
    r = requests.get(href)
    with open(file_name, "wb") as code:
        code.write(r.content)