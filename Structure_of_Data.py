list1 = []
list2 = [1,2,3,4,5]
list3 = ['apple','banana','orange']
list4 = [1,35,16.78,'pineapple']

#print(list2)

#用索引指定串列中的"值"的表示方法
#print(list3[0])

#print(list2[1:3])

#print(list2[0:5])




#增加"值"到串列中
list1.append(10)
#print(list1)

list1.append(20)
#print(list1)

list1.insert(1,30)
#print(list1)


#移除串列中的指定"值"

list2.pop()
#print(list2)

list2.pop(1)
#print(list2)

list2.remove(3)
#print(list2)


#計算串列中"值"的出現次數

list3.append('apple')
print(list3)
print(list3.count('apple'))

求出串列中"值"的索引值
print(list3.index('banana'))
print(list3.index('orange'))


#串列"值"的排序

list1.append(40)
print(list1)

list1.sort()
print(list1)

list1.reverse()
print(list1)


#判斷值是否在/不在串列中 → 回應布林值

print(30 in list1)
print(80 not in list1)


#數學式運算

print(sum(list1))

print(max(list1))

print(min(list1))

print(list1 + list2)

print(2 * list2)

#索引值表示法--負數表示從最後倒數

print(list1[-1])
print(list1[-3:-1])
print(list1[-4:4])


#利用迴圈，一個一個印出列表中的"值"

list3 = ['apple', 'banana', 'orange', 'kiwi']
for i in range(len(list3)):
    print('list3[%d] = %s' % (i,list3[i]))
    print(i , '=' , list3[i])

    
#利用亂數及串列算樂透號碼

#ex1基礎型，號碼會重複
import random
lotto = []
for i in range(1, 7):
    randomNum = random.randint(1,49)
    lotto.append(randomNum)
print('the Num of lottory is:')
for i in lotto:
    print('%4d' % i ,end = '')
    

#利用輔助串列來核對樂透號碼是不是有重複(checkNum)
import random
lotto = []
checkNum = []

for i in range(0, 50): #0~49
    checkNum.append(0) #把checkNum的串列全部變成[0,0,0,0,......,0]

count = 1
while count <= 6:#迴圈重複執行，計數取6次
    randomNum= random.randint(1,49)#取出亂數1~49
    if checkNum == 0:#若核對值=0，則加新亂數進入。若是1，則代表該亂數已經加入到lotto[]
        lotto.append(randomNum)#以取得的亂數作為所引值帶入到chekNum[]串列，判斷該元素是否為0
        count += 1 #完成一次計數
    checkNum[randomNum] = 1 #將checkNum[]串列中該索引值位置之元素設定為1
print('the lottory Num is:\n', end = '')
for i in lotto:
    print(i, end = '')
print()
    

#利用函式判斷數列的值是否重複(not in)
import random
lotto = []
n = 1
while n <= 6:
    randomNum = random.randint(1,49)
    if randomNum not in lotto:
        lotto.append(randomNum)
        n += 1
print('the lottory Num is: \n', end = '')
for i in lotto:
    print('%4d' % i , end = '')
print()


#數值不重複並排序(not in + sort)
import random
lotto = []
n = 1
while n <= 6:
    randomNum = random.randint(1,49)
    if randomNum not in lotto:
        lotto.append(randomNum)
        n += 1
print('the lottory Num is: \n', end = '')
for i in lotto:#利用迴圈印出lotto的號碼
    print('%4d' % i , end = '')
print()
lotto.sort()
print('and line the Num is:')
for i in lotto:
    print('%4d' % i , end = '')
print()


#二維串列

st = [[85,68,73],[96,81,60],[87,77,58],[80,60,60],[78,81,55]]
print(st[0]) #第1位學生的3科成績
print(st[3][0]) #第4位學生的第1科成績  → 二維串列表示法會有2個連續中括號[]

#2x3的串列
list5 = [[1,2,3],[4,5,6]]
print(list5)
print(list5[0])
print(len(list5))
print(len(list5[1]))


#輸入二維元素的數量後，隨機產生該數量的1~50數值
import random
rows = eval(input('plz entert the Num of rows:'))
columns = eval(input('plz enter the Num of columns:'))
lst = [] #先宣告一維串列
for i in range(rows): #準備建立rows數量的空二維值
    lst.append([]) #建立二維串列
    for j in range(columns):#準備輸入'columns個'亂數
        lst[i].append(random.randint(1,50)) #增加亂數值到串列中
print(lst)
print()

#以'lst[][]=值'的方式來表示
for i in range(len(lst)): #設定i迴圈
    for j in range(len(lst[0])): #lst[0]是抓串列的第一行的長度是多少(有多少個)
        print('lst[%d][%d] = %5d' % (i , j , lst[i][j]))
    print()


#加入單位元素串列，並計算總合
    
import random
rows = eval(input('plz entert the Num of rows:'))
columns = eval(input('plz enter the Num of columns:'))
lst = [] 
for i in range(rows): 
    lst.append([])
    for j in range(columns):
        lst[i].append(random.randint(1,50)) 
print(lst)
print()
for row in range(len(lst)):
    total = 0
    for column in range(len(lst[0])):
        total += lst[row][column]
    print('第%d行的總和 = %d' % (row+1,total))


#元祖(數組):tuple

tu1 = (1,2,3,4,5)
print(tu1)

tu2 = ()

#在數組當中放入串列
tu3 = tuple([x for x in range(1,6)])
print(tu3)

#數組當中的字串類型，會被抓出一個一個單一字母
tu4 = tuple('python')
print(tu4)

#在數組中增加元素
tu1 += (6,7)
print(tu1)

#以中括號找索引值
print(tu1[2])
print(tu1[3:6])

#複製數組
tu3 = 2 * tu3
print(tu3)

#用for迴圈個別列印出數組中的元素(分行列印)
for i in tu1:
    print(i,end='\n')
    
#刪除數組(再次要求列印時會顯示該變數未定義)
del tu1
print(tu1)

#資料轉換:
#數組轉串列
tuple1 = (1,2,3)
list1 = list(tuple1)
#串列轉數組
list2 = [4,5,6]
tuple2 = tuple(list2)


#集合:set

#以大括號{}建立
s1 = {1,2,3}
print(s1)
#空集合
s2 = set()
print(s2)
#以串列輸入集合
s3 = set([x for x in range(1,6)])
print(s3)
#以數組輸入集合
s4 = set((1,2,3))
print(s4)

#集合不包含重複資料
s5 set((1,1,2,2,3))
print(s5)

#add(x):將x加入集合
s10 = {1,3,6}
s10.add(20)
s10

#remove(x):將x從集合移除
s10.remove(3)
s10


#聯集:union (可以符號 「|」做運算)
set20 = {1,6,8,10,20}
set25 = {1,3,8,10}
set20.union(set25)
set20|set25

#交集:intersection (可以符號「&」做運算)
set20&set25

#差集:difference (可以符號「-」做運算)
set20-set25

# == :比較兩集合是否相等 (傳回布林値)
# !== :比較兩集合是否不相等


#詞典:dict  (key:value)

#建立詞典
#標準建立法
dict1 = {'Taipei':'101','Paris':'Tower Eiffel','London':'Big Ben'}
print(dict1)

#dict()函式建立法，()函式內的元素以中括號[]括住,鍵與鍵值之間以逗號隔開
dict01 = dict([[key1,value1],[key2,value2],[key3,value3]])

#dict()加'='建立法，()函式內可以寫成key=value
dict02 = dict(key01 = value01, key02 = value02, key03 = value03)#key值不可為數值

#增加鍵值以及值
dict1['Berlin'] = 'wall'
print(dict1)

#搜尋鍵值
print(dict1['Taipei'])

#分別列出所有的鍵值及值
for key in dict1:
    print('%s:%s' % (key,dict1[key]))

#列出key值：keys()
print(dict1.keys())

#列出對應值：values()
print(dict1.values())

#列出項目值：items()
print(dict1.items())

#在執行dict的時候都會有前綴dict，可以以數組來變通
tuple(dict1.items())

#dict的複製和合併
dict2 = {1:'red',2:'yellow',3:'green'}
dict3 = {4:'blue',1:'purple'}
dict4 = dict2.copy() #複製詞典
dict4
dict4.update(dict3) #合併詞典(以dict3來更新dict4)
dict4 #若有相同key值，只取1個


#使用get()取得值
fruit = {'apple':15,'banana':10,'tomato':12}
print(fruit.get('apple'))
print(fruit.get('pineapple'))

#exercise:(未除錯)
dict1 = {'A':'shy and stable','B':'positive and out-going',\
         'O':'strong and self-confidence','AB':'grace and smart'}
dict1.get('O')
blood_type = str.upper(input('enter ur blood type plz:'))
blood_personality = dict1.get(blood_type)
if blood_personality == None:
    print('No such of blood type:'+ blood_type)
else:
    print('The personality of ' + blood_type + ' is ' + str(dict1[blood_type]))
    

#字典修改應用:
dict1 = {'王美麗':85,'王大同':93,'李大年':67}
name = input('輸入學生姓名:')
if name in dict1:
    print(name + '的成績為' + str(dict1[name]))
else:
    score = input('輸入學生成績:')
    dict1[name] = score
    print('成績表為:' + str(dict1))
    

#字串(str)
#建立空字串
s1 = str()
s2 = ''

#建立字串
s3 = str('這是python程式')
s4 = 'Today is rainy day'

len(s4)#字串長度
max(s4)#字串最大值
min(s4)#字串最小值

#[位置數值]:取得字串特定字元
s4[3],s4[-5]

#擷取子字串 → 字串名[起始值:終止值]：取得從起始值開始到終止值-1的子字串
s4[3:8]

#「+」:串接   「*」複製
s5 = 'Simon'
s6 = 'Lee'
s5 + s6
s5 * 2

#字串測試:字串名.函式()，數值傳回皆為布林値

s5.isalnum() #字串是否為字元及數值
s5.isalpha() #字串是否為字元
s5.isdigit() #字串是否為數值
s5.islower() #字串是否為英文小寫
s5.isupper() #字串是否為英文大寫
s5.isspace() #字串是否為空白

#子字串:字串名.函式('子字串')
s5.endswith('on')    #字串尾端是否為'子字串'
s7.startswith('Si')  #字串開頭是否為'子字串'

s5.find('o')   #字串中子字串的最小位置數值
s7 = 'abcDeabcde'
s7.rfind('e')  #字串中子字串的最大位置數值
s7.count('e')  #字串中子字串的個數

#字串轉換:
s8 = '  well come to taipei   '
s8.capitalize() #將字串中第一個字元轉換為大寫，其餘為小寫
s8.lower()      #將字串中所有字元轉換為小寫
s8.upper()      #將字串中所有字元轉換為大寫
s8.swapcase()   #將字串中的字元大寫轉小寫，小寫轉大寫
s8.replace('taipei','tainan')  #將字串s8的'taipei'以字串'tainan'取代
s8.title()      #將字串中每一個單字第一字元轉換為大寫，其餘小寫

#空白處理：字串名.函式()
s8.lstrip()    #刪除字串左側空白
s8.rstrip()    #刪除字串右側空白
s8.strip()     #刪除字串的左右兩側(頭尾)的空白
    
#對齊:
s9 = 'Taipei City'
s9.center(20)   #將字串以寬度值(20)置中對齊
s9.ljust(20)    #將字串以寬度值(20)靠左對齊
s9.rjust(20)    #將字串以寬到值(20)靠右對齊  

#字串分割
s11 = 'apple banana kiwi orange'
s11.split()     #以空白字元作為分割字元(條件)
s12 = '05-22-2020'
s12.split('-')  #指定「-」為分割字元(條件)


#exercise:宣告一整數串列(大小為5)，傳遞給output(aList)函式，函式由輸入初始化後，
#         回傳給主程式並輸出該串列，再由主程式將該串列傳給max(aList)及min(aList),
#         輸出aList之最大值及最小值。(不可使用系統函式)

#exercise：讓使用者輸入10個數字(不重複)至串列，並將該串列傳遞給名為compute()的函式。
#          此函式接收一個串列lst和一個數字a(預設3)，並回傳lst中a個最大的數字，最後
#          再將回傳，結果輸出。
def compute(lst,a = 3):
    lst.sort()
    lst.reverse()
    ans = []
    for i in range(a):
        ans.append(lst[i])
    return ans 

def main():
    lst = []
    n = 1
    while n <= 10:
        num = eval(input('請輸入十個不重複數字:'))
        if num not in lst:
            lst.append(num)
            n += 1
    print(lst)
    print(compute(lst))
main()

#補充，當input想一次輸入數個值的時候，但是慣例上不建議使用，
a, b = map(int,input('請輸入兩個數值:').split(' '))
print(a,b)
#map(要映射處理的「函式」,要映射處理的「值」)
#.split() 把字串中的值用空白鍵分割成獨立元素


#撰寫一程式，以lotto()函式產生大樂透號碼，並以main()函式呼叫5次lotto()函式，即產生
#5組大樂透號碼。請將大樂透號碼由小至大排序。
import random
def lotto():
    lottoLst = []
    n = 1
    while n <= 6:
        randomNum = random.randint(1,49)
        if randomNum not in lottoLst:
            lottoLst.append(randomNum)
            n += 1
    lottoLst.sort()
    print(lottoLst)

def main():
    for i in range(6):
        lotto()
main()

#撰寫一程式，以隨機亂數的方式產生100個介於1~100的亂數，將它置於randLst串列中，然後
#印出第二小的數和第二大的數。(可重複)
import random
def main():
    randLst = []
    for i in range(100):
        randomNum = random.randint(1,1000)
        randLst.append(randomNum) 
    randLst.sort()
        for j in range(1,101):  #j迴圈的功能是在輸出時，以10個數字為一行
            if j % 10 == 0:
                print('%4d' % (randLst[j-1]))
            else:
                print('%4d' % (randLst[j-1]), end = '')   
    second_small_num = int(randLst[1])
    second_big_num = int(randLst[-2])
    print('the second small Num is:%d' % second_small_num, end = '\n' )
    print('the second big Num is:%d' % second_big_num, end = '\n' )
main()

#上述題目，以不重複亂數撰寫。
import random
def main():
    randLst = []
    for i in range(100):
        randomNum = random.randint(1,1000)
        if randomNum not in randLst:
            randLst.append(randomNum)
    randLst.sort()
    second_small_num = int(randLst[1])
    second_big_num = int(randLst[-2])
    print(randLst)
    print('the second small Num is:%d' % second_small_num, end = '\n' )
    print('the second big Num is:%d' % second_big_num, end = '\n' )    
main()
    
