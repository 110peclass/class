# -*- coding: utf-8 -*-
"""
Created on Wed May  6 14:58:23 2020

@author: ASUS
"""

#程式項區分英文大小寫

#程式分行
a = b = c = 12
y = a+ \
    b+ \
    c+ \
    20
print('total=', \
      y)

#程式需區分英文大小寫

#程式敘述以單行為主(一個完整的敘述語法)
#若要在單行撰寫2個以上的完整語法,期間以;隔開每一個完整的敘述
    #如：a=10     → a = 10;b = 20
    #    b=20

#變數：
    #程式中儲存資料供程式使用
    #隨程式執行而變動其值
    #命名規則:
        #1.第一個字元可以是英文大小寫，底線符號，中文不建議。
        #2.其他字元可以是英文大小寫，底線符號，數字，中文不建議。
        #3.英文大小寫視為不同的變數名稱。
        #4.不可使用關鍵字、內建函式、內建類別名稱。
        #5.建議盡量以該變數在程式中代表的功能或意義命名。如age代表年齡。
    #直接寫出使用，不需特別宣告。
    #不須宣告其資料型態、系統會根據變數值自行設定

#資料型別：
    #python採動態型別(無須事先宣告)
    #另有強制型別宣告
    #型別:
        #int→整數
        #float→浮點數
        #bool→布林值:True,False
        #str→字串:以單(雙)引號前後括住
    #相同型別的資料才可以運算
    #python具備自動轉換型別的功能，若無法自動轉換，則採強制型別宣告:
        #使用下列函式:int()、float()、str()
    #type()→查詢資料型態
        x = 10
        y = x/3
        print(x)
        print(type(x))
        print(y)
        print(type(y))
    
        X = 10
        print(X)
        print(type(X))
        X = X+5.5
        print(X)
        print(type(X))
    #int()→預設為十進位，另有
        #二進位→數值前加上0b→轉換函數:bin()
        #八進位→數值前加上0o→轉換函數:oct()
        #十六進位→數值前加上0x→轉換函數:hex()
        num_x = 0b01000001
        print(num_x)
        num_y = 65
        print(bin(num_y))
        num_x_2 = 0o101
        print(num_x_2)
        num_y_2 = 0x41
        print(num_y_2)
        print(hex(255),oct(255),bin(255))
        

#test_input()

score_test1 = input('please enter your CHinese score:')
print(score_test1)


#test_eval()將字串轉為數值

score_test2 = eval(input('please enter your CHinese score:'))
sum_score = score_test2 + 20
print(sum_score)

print(score_test1,sum_score, sep='&', end='\n')


#print():輸出(格式化)
#print('type1 type2 type3...' % (data1,data2,data3...))
# %s_字串 ; %5s → 輸出5個字元，小於5個字元，於左方填入空白，大於5個則全數輸出。
# %d_整數 ; %5d → 輸出5位整數，不足5位左填入空白，超過則全數輸出。
#%f_浮點數 ; %8.2f → 輸出8位數字(含小數點)，小數2位。
import math
print('pi=%20.25f' % math.pi)

#+:於格式數值前方加入，若數值為正，則在資料左方加上'+'
#-:輸出格式資料空間有多餘時，則資料靠左對齊


#格式參數
a = 100
print('a=/%-6d/' % a)
b = 12.3
print('b=/%-6.2f/' % b)
c = 'deep'
print('c=/%-6s/' % c)
d = 50
print('d=/%+6d/' % d)
e = 13.3
print('e=/%+6.2f/' % e)

#練習題(再想一下輸入值如何格式化為數值)
score_ch = input('請輸入國文成績:')
score_en = input('請輸入英文成績:')
score_math = input('請輸入數學成績:')
score_sum = int(score_ch)+int(score_en)+int(score_math)
score_avg = score_sum/3
print('總成績:%d 平均成績:%5.2f' % (score_sum,score_avg))

#流程控制
#if 條件式: →(這個的結果必須為布林值)
#    敘述式 →(若條件成立，則執行敘述式)
num_1 = eval(input('請輸入數值:'))
if (int(num_1) < 0):
    num_1 = abs(num_1)
print('絕對值是:%d' % num_1)

#if 條件式:
#   敘述式1
#else:
#   敘述式2
num_2 = eval(input('請輸入任一整數值:'))
rem_2 = num_2 % 2
if (rem_2 == 0):
    print('%d 是偶數' % num_2)
else:
    print('%d 是基數' % num_2)

#練習:輸入圓半徑，計算出園面積。輸出如右：圓半徑= ,圓面積=。
    #若輸入半徑為負數，則輸出圓半徑不能為負數。
import math
r = int(input('請輸入圓半徑:')) #int改為eval
if r > 0:
    print('圓半徑=',r,',圓面積=',r**2*math.pi)
else:
    print('圓半徑不能為負數')

#if~elif逐一判斷條件，若成立則執行，若所有條件都不成立，則執行n+1
#if 條件1:
#   敘述1
#   elif 條件2:
#   敘述2
#   ...
#else: 敘述n+1
a , b , c = eval(input('請輸入a,b,c的值:'))
d = b**2 -4*a*c
if d>0:
    print('一元二次方程式有兩個不同的解')
elif d == 0:
    print('一元二次方程式只有一個解')
else:
    print('一元二次方程式無解')
print('結束')

#練習
score_class = eval(input('score:'))
if score_class >= 90:
    print('優等')
elif score_class >= 80:
    print('甲等')
elif score_class >= 70:
    print('乙等')
elif score_class >= 60:
    print('丙等')
else:
    print('不及格')
    
#練習
num_3 = eval(input('請輸入1~5的整數:'))
if num_3 == 1:
    print('one')
elif num_3 == 2:
    print('two')
elif num_3 == 3:
    print('three')
elif num_3 == 4:
    print('four')
else:
    print('five')

#練習巢狀if
score_class_1 = eval(input('請輸入成績0~100:'))
if score_class_1 >= 90:
    print('優等')
else:
    if score_class_1 >= 80:
        print('甲等')
    else:
        if score_class_1 >= 70:
            print('乙等')
        else:
            if score_class_1 >= 60:
                print('丙等')
            else:
                print('不及格')

#for
for i in range(11):
    print(i,end='\n')

#計算1+2+3+...+100
sum_s = 0
for s in range(101):
    sum_s += s # sum_s = sum_s + s
print(sum_s)


sum_s = 0
for s in range(101):
    sum_s += s # sum_s = sum_s + s
else:#當上述條件(迴圈)執行完時，執行下列
    print(sum_s)

#連加公式
link_n = eval(input('請輸入整數:'))
sum_n = 0
for link_m in range(1,link_n+1):
    sum_n += link_m
print(sum_n)

#練習4+9+13+18+22+...+85+90+94+99
sum_a = 0
for test_a in range(4,95,9):
    sum_a += test_a
for test_a in range(9,100,9):
    sum_a += test_a
print(sum_a)

#另一個迴圈解法
sum_b = 0
for test_b in range(4,95,9):
    sum_b = sum_b+test_b+(test_b+5)
print(sum_b)

#請輸入正整數:6 6!=720
num_x = eval(input('請輸入正整數:'))
times_z = 1
for num_y in range(1,num_x+1):
    times_z = times_z*num_y
print(times_z)


#while 表示0~10
i = 0
while i < 10:
    i = i+1 
    print(i) #print在下面有差別嗎
    
#請輸入第O位學生成績(輸入)-1結束:
#    ...
#本班總成績:00分，平均:00分(小數2位) 
total = person = score = 0
while(score != -1):
    person += 1 #第幾位學生
    total += score #第0位學生開始的分數+總分=新的總分
    score = int(input('請輸入第 %d 位的學生成績(輸入-1結束)' % person))
average = total / (person -1)
print('本班成績: %d 分，平均成績: %5.2f 分' % (total, average))

#字串物件.upper() → 將字串轉換為全部大寫

#字串物件
ans = input('請輸入電腦的英文:')
while ans.upper() != 'COMPUTER':
    ans = input('答錯了，請重新輸入')
else:
    print('恭喜答對了')
    
#練習作業
age = eval(input('請輸入年齡:'))
ticket = 100
if age <= 7 or age >= 80:
    print(ticket * 0.2,'元')
elif age <= 12 or age >= 70:
    print(ticket * 0.5, '元')
else:
    print(ticket, '元')
   
#九九乘法表第一種寫法
for i in range(1,10):
    for j in range(1,10):
        print('%d x %d = %d' % (i,j,i*j),end='\t')
print()

#九九乘法第二種寫法
for i in range(1,10):
    for j in range(1,10):
        print(i,'x',j,'=',i*j,end='\t')
print()

#九九乘法表第三種寫法
for i in range(1,10):
    for j in range(1,10):
        s = i * j
        if s < 10:
            s = '0'+ str(s)
        print(i, 'x', j, '=', s,sep='',end='\t')
print()

#中斷迴圈break
ans = input('請輸入電腦的英文(輸入QUIT結束):')
while ans.upper() != 'COMPUTER':
    if ans.upper() == 'QUIT':
        print('不玩了!')
        break
    ans = input('答錯了，請重新輸入電腦的英文(輸入QUIT結束):')
else:
    print('恭喜答對了')
    
#輸入大樓的樓層數，並印出樓層數，請排除4樓
floor = int(input('請輸入本大樓的樓層數:'))
print('本大樓具有的樓層為:')
if floor > 3:
    floor += 1
for i in range(1,floor+1):
    if i == 4:
        continue
    print(i,end='\n')
print()
    
#輸入層數
#以*列印出該層數的直角三角形
n = eval(input('請輸入直角三角形層數:'))
for a in range(1,n+1):     #外迴圈定義全層數
    for b in range(0,a):   #內迴圈定義B在a的條件下每一層的結果
        print('*',end='')  #內迴圈打印*號並且不換行直到結束
    print()                #內圈跑完出外迴圈時換行

#練習題
#輸入層數，以*列印出該層數的直角三角形(反過來的)
n = eval(input('請輸入直角三角形層數:'))
for a in range(1,n+1):     
    c = n+1-a            #給第三個變數使範圍顛倒
    for b in range(0,c):   
                print('*',end='')  
    print()                

#練習題老師解，倒過來的直角三角形
n = eval(input('請輸入直角三角形層數:'))
for a in range(n,-1,-1): 
    for b in range(1,a+1):  
        print('*',end='')  
    print() 
    
#輸入三位整數，判斷此三位數是否為回文數
num_c = eval(input('輸入三位整數:'))
rev_num_c = (num_c % 10) * 100 + (num_c // 10 % 10) * 10 \
    + (num_c // 100)
if num_c == rev_num_c:
    print(num_c,'是回文數')
else:
    print(num_c,'不是回文數')
    
#數列排序
num_1, num_2, num_3 = eval(input('enter three integrers:'))
if num_1 > num_2:
    num_1, num_2 = num_2, num_1
if num_2 > num_3:
    num_2, num_3 = num_3, num_2
if num_1 > num_2:
    num_1, num_2 = num_2, num_1
print('The sorted numbers are:', num_1, num_2, num_3)

#亂數產生器
import random
for r in range(1,11):
    r_n = random.randint(1,100)
    print('%4d' % r_n, end='')

#練習題，取10個隨機數，求幾個奇數、幾個偶數。
import random
for r in range(1,11):
    r_n = random.randint(1,100)
    print('%4d' % r_n, end='')
    if r_n % 2 == 0:
        r_s = 0
        r_d = 0
        r_s += 1
    r_d = 10 - r_s
print('\n偶數 = %d 個, 奇數 = %d 個 ' % (r_d,r_s))

#打印n個*號
for i in range(1,21):
    print('*', end='')
print()
for i in range(1,31):
    print('*', end='')
print()
for i in range(1,51):
    print('*', end='')
print()

#改寫定義程式
def printstart(n):
    for i in range(1,n+1):
        print('*', end='')
    print()
def main():#集中呼叫，結構化
    printstart(20)
    printstart(30)
    printstart(50)
main()

#練習題：建立函式，求出1~100總和(無參數、無傳回值)
def total_n():
    n = 0
    for i in range(1,101):
        n += i
    print(n)
total_n()

#練習題：建立函式，求出1~100總和(無參數、有傳回值)
def total_n():
    n = 0
    for i in range(1,101):
        n += i
    return n
def main():  #運用呼叫
    t = total_n() #因為有傳回值，所以先給一個變數
    print(t)
main()

#練習題：輸入兩個數值，求出連加和。(有參數，有傳回值)
def total_s(a,b):
    sum = 0
    for i in range(a,b+1):
        sum += i
    return sum
def main():
    x, y = eval(input('請輸入兩個值，求連加和:'))
    t = total_s(x, y)
    print('%d 到 %d的連加和為:%d' % (x, y ,t))
main()    
    
#練習題:求兩數連加和以及平均數。(多個傳回值)還沒寫好！
def total_s(a,b):
    sum = 0
    for i in range(a,b+1):
        sum += i
    return sum
def main():
    x, y = eval(input('請輸入兩個值，求連加和:'))
    t = total_s(x, y)
    avg = t/(y-x+1)
    print('%d 到 %d的連加和為:%d,平均為:%0.2f' % (x, y ,t, avg))
main()    

#預設參數值
def sum_avg(n1, n2=100):
    total = 0
    avg = 0.0
    for i in range(n1, n2+1):
        total += i
    avg = total / (n2 - n1 + 1)
    return total , avg

def main():
    s , a = sum_avg(1)
    print('sum= %d,avg = %0.2f' % (s , a))
    s , a = sum_avg(1 , 10)
    print('sum= %d,avg = %0.2f' % (s , a))

main()

#請輸入攝氏溫度:36，=華氏96.8度。
#無傳回值
def fa_d():
    c = eval(input('請輸入攝氏溫度:'))
    fra = c * 9/5 + 32
    print('攝氏溫度 %d 度 =華式 %0.2f 度' % (c, fra))
fra_d()
#有傳回值
def fa_d(c):    
    fra = c * 9/5 + 32
    return fra
c = eval(input('請輸入攝氏溫度:'))
print('攝氏溫度 %d 度 =華式 %0.2f 度' % (c, fra))