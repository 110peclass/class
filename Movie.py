import pandas as pd
from os import walk
from os.path import join
import csv

def getCSV():   #建立自定義函式，下載目標所有CSV檔
    import requests
    from bs4 import BeautifulSoup
    import re
    html = requests.get('https://data.gov.tw/dataset/94224')    #取得網頁文本
    bsobj = BeautifulSoup(html.content, 'lxml')     #建立樹狀元素物件

    #定位下載頁面的div，用回圈下載每一個CSV並命名
    for single_div in bsobj\
            .find('div', {'class': 'field field-name-field-dataset-resource field-type-dgresource-resouce field-label-inline clearfix'})\
            .find('div', {'class', 'field-items'})\
            .findAll('a', string='CSV'):

        href = single_div.get('href')   #取得下載連結
        currency_name = single_div.parent.find('span', {'class', 'ff-desc'}).text   #取得標籤名稱做為要下載的檔名
        currency_name = re.sub('[全國電影票房.]', '', currency_name)

        file_name = r'D:\Users\Vanity\Desktop\Python\Data_exercise\Movie\m' + currency_name + '.csv'
        r = requests.get(href)
        with open(file_name, "wb") as code:
            code.write(r.content)

# getCSV()


def csvSolve(df):   #建立自定義函式，處理欄位名稱與value的轉數值
    #捨棄不要的欄位
    dropColumns = ['序號', '申請人', '發行', '累計銷售金額', '累計銷售票數', '上映日數', '累計票數', '累計金額']
    dropMask = df.columns.isin(dropColumns)
    df = df.drop(df.columns[dropMask], axis=1)

    #把欄位名稱「周票數變動率」、「週票數變動率」統一為變動率，並對原數據無該欄位的df做新增欄位
    reColMask = [col for col in df.columns if '變動' in col]
    try:
        reCol = reColMask[0]
        df = df.rename(columns={reCol: '變動率'})
    except:
        df['變動率'] = '0'

    #將value轉態，字串轉數值
    df.loc[:,'銷售票數'] = df.loc[:,'銷售票數'].apply(lambda x: ''.join([i for i in x if i.isdigit()])).astype(int)
    df.loc[:,'變動率'] = df.loc[:, '變動率'].str.replace('%','').str.replace(',','').astype(float).apply(lambda x: x/100)
    df.loc[:,'銷售金額'] = df.loc[:,'銷售金額'].apply(lambda x: ''.join([i for i in x if i.isdigit()])).astype(int)

    #重新排序欄位
    df = df[['國別地區','中文片名','上映日期','出品','上映院數','銷售票數','銷售金額','變動率']]
    return df


mypath = r"D:\Users\Vanity\Desktop\Python\Data_exercise\Movie"  # 絕對路徑
for root, dirs, files in walk(mypath):  # 從目標路徑的資料夾中找出每一個根目錄root、資料夾dirs、檔案files
    for f in files:  # 用迴圈讀取每一個檔案
        fullpath = join(root, f)
        # fullpath= 資料夾根目錄root+檔案名稱f
        # 將f檔案名稱，去除部分字元，並將日期各位數部分補0。
        name = re.sub('[至m.csv]','',f)
        name = re.split(r'[年月日]',name)[0:3]
        dfName = ''  
        for i in name:
            if len(i) < 2:
                i = '0'+i
            dfName += i

        globals()['df' + f.replace('.csv', '')] = pd.read_csv(fullpath)
        globals()['df' + f.replace('.csv', '')] = csvSolve(globals()['df' + f.replace('.csv', '')])
