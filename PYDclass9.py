#PYD901

#1. 題目說明:
#請開啟PYD901.py檔案，依下列題意進行作答，使輸出值符合題意要求。作答完成請另存新檔
#為PYA901.py再進行評分。
#
#請注意：程式碼中所提供的檔案路徑，不可進行變動，write.txt檔案需為UTF-8編碼格式。
#
#2. 設計說明：
#請撰寫一程式，將使用者輸入的五筆資料寫入到write.txt（若不存在，則讓程式建立它），
#每一筆資料為一行，包含學生名字和期末總分，以空白隔開。檔案寫入完成後要關閉。
#
#3. 輸入輸出：
#輸入說明
#五筆資料（每一筆資料為一行，包含學生名字和分數，以空白隔開）
#
#輸出說明
#將輸入的五筆資料寫入檔案中，不另外輸出於頁面
#
#輸入輸出範例
#範例輸入
#Leon 87
#Ben 90
#Sam 77
#Karen 92
#Kelena 92

f_w = open(r'C:\Users\ASUS\Desktop\109100112\Python\TQC+\write.txt','w')
for i in range(5):
    data = f_w.write(input())
f_w.close

###############################################################################

#PYD902

#1. 題目說明:
#請開啟PYD902.py檔案，依下列題意進行作答，使輸出值符合題意要求。作答完成請另存新檔
#為PYA902.py再進行評分。
#
#請注意：資料夾或程式碼中所提供的檔案路徑，不可進行變動，read.txt檔案需為UTF-8編碼
#格式。
#
#2. 設計說明：
#請撰寫一程式，讀取read.txt的內容（內容為數字，以空白分隔）並將這些數字加總後輸出。
#檔案讀取完成後要關閉。
#
#3. 輸入輸出：
#輸入說明
#讀取read.txt的內容（內容為數字，以空白分隔）
#
#輸出說明
#總和
#
#輸入輸出範例
#範例輸入
#無
#
#範例輸出
#660

f = open('C:\\Users\\ASUS\\Desktop\\109100112\\Python\\TQC+\\read902.txt', 'r')
data = f.read()
f.close()

num = data.split(' ')
total = 0 
for i in range(0, len(num)):
    total += eval(num[i])

print(total)

###############################################################################

#PYD904

#1. 題目說明:
#請開啟PYD904.py檔案，依下列題意進行作答，使輸出值符合題意要求。作答完成請另存新檔
#為PYA904.py再進行評分。
#
#請注意：資料夾或程式碼中所提供的檔案路徑，不可進行變動，read.txt檔案需為UTF-8編碼
#格式。
#
#2. 設計說明：
#請撰寫一程式，讀取read.txt（每一列的格式為名字和身高、體重，以空白分隔）並顯示檔
#案內容、所有人的平均身高、平均體重以及最高者、最重者。
#
#提示：輸出浮點數到小數點後第二位。
#
#3. 輸入輸出：
#輸入說明
#讀取read.txt（每一行的格式為名字和身高、體重，以空白分隔）
#
#輸出說明
#輸出檔案中的內容
#平均身高
#平均體重
#最高者
#最重者
#
#輸入輸出範例
#範例輸入
#無
#
#範例輸出
#Ben 175 65
#
#Cathy 155 55
#
#Tony 172 75
#Average height: 167.33
#Average weight: 65.00
#The tallest is Ben with 175.00cm
#The heaviest is Tony with 75.00kg

data = []

with open('C:\\Users\\ASUS\\Desktop\\109100112\\Python\\TQC+\\read904.txt', 'r') \
        as file:
    for line in file:
        print(line)
        
        tmp = line.strip('\n').split(' ')
        tmp = [tmp[0], eval(tmp[1]), eval(tmp[2])]  
#        ↑['Ben', 175, 65]['Cathy', 155, 55]['Tony', 172, 75]
        data.append(tmp)    #將三筆資料逐筆加入成3x3的二為串列

name = [data[x][0] for x in range(len(data))]
height = [data[x][1] for x in range(len(data))]
weight = [data[x][2] for x in range(len(data))]

print('Average height: %.2f' % (sum(height)/len(height))) #
print('Average weight: %.2f' % (sum(weight)/len(weight)))

max_h = max(height)
max_w = max(weight)
print('The tallest is %s with %.2fcm' % (name[height.index(max_h)], max_h))
print('The heaviest is %s with %.2fkg' % (name[weight.index(max_w)], max_w))

###############################################################################

#PYD906

#1. 題目說明:
#請開啟PYD906.py檔案，依下列題意進行作答，使輸出值符合題意要求。作答完成請另存新檔
#為PYA906.py再進行評分。
#
#請注意：資料夾或程式碼中所提供的檔案路徑，不可進行變動，data.txt檔案需為UTF-8編碼
#格式。
#
#2. 設計說明：
#請撰寫一程式，要求使用者輸入檔名data.txt、字串s1和字串s2。程式將檔案中的字串s1以
#s2取代之。
#
#3. 輸入輸出：
#輸入說明
#輸入data.txt及兩個字串（分別為s1、s2，字串s1被s2取代）
#
#輸出說明
#輸出檔案中的內容
#輸出取代指定字串後的檔案內容
#
#輸入輸出範例
#範例輸入
#data.txt
#pen
#sneakers
#範例輸出
#=== Before the replacement
#watch shoes skirt
#pen trunks pants
#=== After the replacement
#watch shoes skirt
#sneakers trunks pants

f_name = input()
str_old = input()
str_new = input()

infile = open(f_name,'r')
data = infile.read()

print('=== Before the replacement')
print(data)
infile.close

print('=== After the replacement')  
new_data = data.replace(str_old, str_new)
print(new_data)

outfile = open(f_name, 'w')
outfile.write(new_data)
outfile.close  

###############################################################################

#PYD908

#1. 題目說明:
#請開啟PYD908.py檔案，依下列題意進行作答，使輸出值符合題意要求。作答完成請另存新檔
#為PYA908.py再進行評分。
#
#請注意：資料夾或程式碼中所提供的檔案路徑，不可進行變動，read.txt檔案需為UTF-8編碼
#格式。
#
#2. 設計說明：
#請撰寫一程式，要求使用者輸入檔名read.txt，以及檔案中某單字出現的次數。輸出符合次
#數的單字，並依單字的第一個字母大小排序。（單字的判斷以空白隔開即可）
#
#3. 輸入輸出：
#輸入說明
#讀取read.txt的內容，以及檔案中某單字出現的次數
#
#輸出說明
#輸出符合次數的單字，並依單字的第一個字母大小排序
#
#輸入輸出範例
#範例輸入
#read.txt
#3
#範例輸出
#a
#is
#programming

f_name = input()#C:\\Users\\ASUS\\Desktop\\109100112\\Python\\TQC+\\read908.txt
n = int(input())
word_dict = dict()

with open(f_name,'r') as file:              #讀取檔案並命名為file
    for line in file:                       #以迴圈逐一讀取file內容
        word = line.strip('\n').split(' ')  #去除換行(\n)，以空格分割設定為word
        
        for x in word:                      #利用迴圈，將每一個單字取出計數，並做成字典
            if x in word_dict:              #key → x=word ; value → 次數
                word_dict[x] += 1           #單字出現過就把value+1
            else:
                word_dict[x] = 1            #第一次出現的單字.就把value先給1
            
word_list = word_dict.items()               
#取出字典中所有key及value設定給word_list串列
#dict_items([('What', 1), ('is', 3), ('Python', 4),......

wordQTY = [x for (x,y) in word_list if y == n] 
#找出在word_list中的x值，當value=3的時候 → ['is', 'a', 'programming']

sortedword = sorted(wordQTY)                #再將取出的值排序(串列狀態)

for x in sortedword:                        #取出排序串列中的值
    print(x)
    
#PYD908第二解

fn = input()
n = int(input())
with open(fn,'r',encoding='UTF-8') as fp:   #讀全檔並以UTF-8編碼
    data = sorted(fp.read().split())        #內容以「空白分割」後「排序」

for i in sorted(set(data)):                 #把串列改為集合，並再次排序
    if data.count(i) == n:                  #將串列中，計數為3次的取出
        print(i)                            #列印在"集合"中的i值(集合資料不重複)
        
    

    
    

###############################################################################

#PYD910

#1. 題目說明:
#請開啟PYD910.py檔案，依下列題意進行作答，使輸出值符合題意要求。作答完成請另存新檔
#為PYA910.py再進行評分。
#
#請注意：資料夾或程式碼中所提供的檔案路徑，不可進行變動，read.dat檔案為UTF-8編碼格
#式。
#
#2. 設計說明：
#請撰寫一程式，要求使用者讀入read.dat（以UTF-8編碼格式讀取），第一列為欄位名稱，第
#二列之後是個人記錄。請輸出檔案內容並顯示男生人數和女生人數（根據"性別"欄位，0為女
#、1為男性）。
#
#3. 輸入輸出：
#輸入說明
#讀取read.dat
#
#輸出說明
#讀取檔案內容，並格式化輸出男生人數和女生人數
#
#輸入輸出範例
#範例輸入
#無
#
#範例輸出
#學號 姓名 性別 科系
#
#101 陳小華 0 餐旅管理
#
#202 李小安 1 廣告
#
#303 張小威 1 英文
#
#404 羅小美 0 法文
#
#505 陳小凱 1 日文
#Number of males: 3
#Number of females: 2

f_name = 'C:\\Users\\ASUS\\Desktop\\109100112\\Python\\TQC+\\read.dat'
c_male = c_female = 0

with open(f_name, 'rb') as file:            #rb→以二進位方式開啟
    for line in file:
        row = line.decode('utf-8')          #decode(編碼格式)，以utf-8格式讀取
        print(row)                          #把資料以row輸出(4筆橫著)
        row = row.strip('\n').split(' ')    #把資料處理成(4x4)的二維串列
        print(row)
        
        if row[2] == '1':
            c_male += 1
        elif row[2] == '0':
            c_female += 1

print('Number of males: %d' % c_male)
print('Number of females: %d' % c_female)
        