#檔案存取及例外處理

def main():
    outfile = open('C:\\Users\\ASUS\\Desktop\\109100112\\Python\\fruits.txt','w')
    outfile.write('Banana\n')
    outfile.write('Grape\n')
    outfile.write('Orange\n')
    outfile.write('蘋果\n')
    outfile.write('芒果\n')
    outfile.close()
main()

infile = open('C:\\Users\\ASUS\\Desktop\\109100112\\Python\\fruits.txt','r')
print('使用readline()方法:')
line1 = infile.readline()
line2 = infile.readline()
line3 = infile.readline()
line4 = infile.readline()

print(repr(line1))
print(repr(line2))
print(repr(line3))
print(repr(line4))

print()
print(line1)
print(line2)
print(line3)
print(line4)
infile.close()

infile = open('C:\\Users\\ASUS\\Desktop\\109100112\\Python\\fruits.txt','r')
line1 = infile.read()
print('使用read()方法:')
print(repr(line1))
print(line1)
infile.close()

infile = open('C:\\Users\\ASUS\\Desktop\\109100112\\Python\\fruits.txt','r')
print('\n使用 readlines()方法:')
line1 = infile.readlines()
print(line1)
infile.close()

def main():
    infile = open('C:\\Users\\ASUS\\Desktop\\109100112\\Python\\fruits.txt','r')
    print('使用read(3)方法:')
    line1 = infile.read(3)
    print(repr(line1))
    
    print('read(8)方法:')
    line2 = infile.read(8)
    print(repr(line2))
    infile.close
main()

def main():
    infile = open('C:\\Users\\ASUS\\Desktop\\109100112\\Python\\fruits.txt','r')
    line = infile.readline()
    while line != '':
        print(line)
        line = infile.readline()
    infile.close()
main()

def main():
    outfile = open('C:\\Users\\ASUS\\Desktop\\109100112\\Python\\cities.txt','w')
    outfile.write('Taipei\n')
    outfile.write('London\n')
    outfile.write('Conventry\n')
    outfile.close()
    
    infile = open('C:\\Users\\ASUS\\Desktop\\109100112\\Python\\cities.txt','r')
    data= infile.read()
    print(data)
    infile.close()
main()

def main():
    outfile = open('C:\\Users\\ASUS\\Desktop\\109100112\\Python\\cities.txt','w+')
    outfile.write('Taipei\n')
    outfile.write('London\n')
    outfile.write('Conventry\n')
    outfile.seek(0,0) #把指標指向檔頭起始點，以便執行完上述write後，從頭執行read。
    data = outfile.read()
    print(data)
    outfile.close()
main()

#當執行完write指令後，指標是指向檔尾。

#使用二進位檔案存取
import pickle
def main():
    outbinfile = open('C:\\Users\\ASUS\\Desktop\\109100112\\Python\\binaryfile.dat','wb')
    pickle.dump(123, outbinfile)
    pickle.dump(77.7, outbinfile)
    pickle.dump('Python is good programing', outbinfile)
    pickle.dump([11,22,33], outbinfile)
    outbinfile.close()
    
    inbinfile = open('C:\\Users\\ASUS\\Desktop\\109100112\\Python\\binaryfile.dat','rb')
    print(pickle.load(inbinfile))
    print(pickle.load(inbinfile))
    print(pickle.load(inbinfile))
    print(pickle.load(inbinfile))
    inbinfile.close()
main()

#pickle.dump(寫入的資料,寫入的檔案)
#pickle.load(讀取的檔案)
import pickle
def main():
    outfile = open('C:\\Users\\ASUS\\Desktop\\109100112\\Python\\score.txt','wb')
    data = eval(input('請輸入整數，輸入0結束:'))
    while data != 0:
        pickle.dump(data, outfile)
        data = eval(input('請輸入整數，輸入0結束:'))
    outfile.close()
    
    infile = open('C:\\Users\\ASUS\\Desktop\\109100112\\Python\\score.txt','rb')
    end_of_file = False
    while not end_of_file:
        try:
            print(pickle.load(infile), end = ' ')
        except EOFError: #End Of File Error 檔案結尾錯誤
            end_of_file = True
    infile.close()
    print('所有資料已讀\n')
main()

def main():
    try:
        n1, n2 = eval((input('請輸入兩個值,以逗號分開:')))
        ans = n1 / n2
        print('%d / %d = %0.5f' % (n1, n2, ans))
    except ZeroDivisionError:
        print('除法分母不可為0')
    except SyntaxError:
        print('輸入資料必須以逗號隔開')
    except:
        print('輸入時發生錯誤')
    else:
        print('沒有異常錯誤')
    finally:
        print('fially子句被執行')
main()

#except type 異常形態
EOFError            #檔案結尾讀取錯誤
ZeroDivisionError   #除法分母為0錯誤
SyntaxError         #符號錯誤

#exercise 1 撰寫一程式，以不定數回圈輸入學生姓名、微積分與會計成績。當輸入學生姓
#           名為none時，則結束輸入動作，必將上述的輸入資料寫入名為students.dat的
#           檔案中。
outfile = open('C:\\Users\\ASUS\\Desktop\\109100112\\Python\\students.dat','w')
data_name = input('請輸入學生姓名:')
while data_name != none:
    try:
        
            outfile.write(data_name)
            outfile.write(input([,]))
        else:
            
    except:
        
#參考寫法
outfile = open('C:\\Users\\ASUS\\Desktop\\109100112\\Python\\students.dat','w')
while True:
    name = input('請輸入學生姓名:')
    calculus = input('請輸入微積分成績:')
    accounting = input('請輸入會計成績:')
    if name == 'none':
        break
    else:
        outfile.write(name)
        outfile.write(' ')
        outfile.write(calculus)
        outfile.write(' ')
        outfile.write(accounting)
        outfile.write(' ')
        outfile.write('\n')
outfile.close()

#題組2：承上題，請讀取students的資料
infile = open('C:\\Users\\ASUS\\Desktop\\109100112\\Python\\students.dat','r')
line = infile.readline()
while line != '':
    print(line)
    line = infile.readline()
infile.close

#題組3：將學生的成績算出平均，微積分的比重60%，會計的比重40%。
infile = open('C:\\Users\\ASUS\\Desktop\\109100112\\Python\\students.dat','r')
data = infile.readline()
while data != '':
    data_score = str.split(data) #與解答不同 data.split(' ')
#    print(data_score)
    calculus = eval(data_score[1])
    accounting = eval(data_score[2])
    average = calculus * 0.6 + accounting * 0.4
    print('%5s:%0.2f' % (data_score[0], average))
    data = infile.readline()
infile.close()

#題組4：找出微積分成績最高的學生。
infile = open('C:\\Users\\ASUS\\Desktop\\109100112\\Python\\students.dat','r')
data = infile.readline()
max = 1 
while data != '':
     temp = data.split(' ')
     calculus = eval(temp[1])
     if calculus > max:
         max = calculus
         name = temp[0]
     data = infile.readline()
print('微積分分數最高者為 %5s: %3d' % (name, max))
infile.close()
    
#max變數設定 → 給定初始值
#calculus → 比較大小 max (參考值)
#若calculus > max  ，則將calculus 指定給max。
#讀入下一個calculus值， 繼續與新的max比較

#題組5：找出會計成績最低的學生。(檢驗有誤)
infile = open('C:\\Users\\ASUS\\Desktop\\109100112\\Python\\students.dat','r')
data = infile.readline()
min = 101
while data != '':
    temp = data.split(' ')
    accounting = eval(temp[2])
    if accounting < min:
        min = accounting
        name = temp[0]
    data = infile.readline()
print('會計成績最低分的學生是 %5s : %3d' % (name,accounting))
infile.close()

#exercise2：撰寫一程式，以不定數迴圈要求使用者輸入字串，檢視若字串是以B(大寫)字元
#           開頭，則將此字串，則將此字串加入到lst串列中，最後將其印出。當使用者輸
#           入end時，將結束輸入的動作。

def main():
    lst = []
    while True:
        str = input('請輸入字串:')
        if str != 'end':
            if str.startswith('B'):
                lst.append(str)
        else:
            break
    print(lst)
main()

#exercise3：撰寫一程式，輸入一個名為str的字串與欲尋找的字串，將找到的字串以「Bright」
#           字串取代。若沒有找到欲找尋的字串，則印出「is not found」的訊息。
def main():
    str = input('請輸入字串:')
    str_find = input('請輸入欲尋找的字串')
    fstr = str.find(str_find)
    if fstr != -1:
        endstr = str.replace(fstr, 'Bright')
        print(endstr)
    else:
        print(fstr + 'is not found')
        
        len_
    
