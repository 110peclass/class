import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.pyplot import MultipleLocator
import numpy as np
import chartify as char

pd.set_option("display.max_columns", None)

df_js = pd.read_json(r'CriminalByAll96_107.json')
df_json = df_js.copy()

df_json.iloc[4:12,[5,6,7,8,11,12,13,14]]=df_json.iloc[4:12,[5,6,7,8,11,12,13,14]].astype(int)
df_json.columns = ['年', '總計男', '總計女', '竊盜男', '竊盜女', '一般傷害男', '一般傷害女',
                   '詐欺背信男', '詐欺背信女', '毒品男', '毒品女', '公共危險男', '公共危險女', '其他男', '其他女']
df_json['年'] = df_json['年'].apply(lambda x: x - 1911)
# print(df_json)
describe_mainAll = df_json.describe()
print(describe_mainAll)

# 96~107年歷年刑事案件總計堆疊圖(年分/性別)______________________________________________________
#
df_jsonByAllM = pd.DataFrame(df_json, columns=['年', '總計男', ]).copy()
df_jsonByAllM['genders'] = '男性'
df_jsonByAllM = df_jsonByAllM.rename(columns={'總計男': 'num'})
df_jsonByAllF = pd.DataFrame(df_json, columns=['年', '總計女', ]).copy()
df_jsonByAllF['genders'] = '女性'
df_jsonByAllF = df_jsonByAllF.rename(columns={'總計女': 'num'})
df_jsonByAll = pd.concat([df_jsonByAllM, df_jsonByAllF])
average101_107 = df_json.iloc[0:11,1:3].sum() /12

# ch = (char.Chart(blank_labels=True)
#     .axes.set_xaxis_range(95,108)
#     .axes.set_yaxis_range(0,60000)
#     .callout.box( left=101, right=106, alpha=0.3, color='yellow')
#     .callout.line(42503, line_dash='dashed')
#     .callout.text('平均總案數=42,503',106,42650)
# )
# ch.plot.area(
#     data_frame=df_jsonByAll,
#     x_column='年',
#     y_column='num',
#     color_column='genders',
#     stacked=True)
#
# ch.set_title('96至107年歷年刑事案件總計堆疊圖')
# ch.set_subtitle('年分/性別分計')
# ch.set_source_label('Data.Taipei')
# ch.axes.set_xaxis_label('民國年分')
# ch.axes.set_yaxis_label('男性、女性數量')
# ch.set_legend_location('outside_bottom')
# ch.plot.text(
#     data_frame=df_jsonByAll,
#     x_column='年',
#     y_column='num',
#     text_column='num',
#     color_column='genders',
#     x_offset=1,
#     y_offset=-1,
#     font_size='10pt')
#
# ch.show()

#100到107年案類趨勢線圖
dfItem100_107 = pd.DataFrame(df_json['年'].iloc[4:12].copy())
dfItem100_107['竊盜'] = df_json['竊盜男']+df_json['竊盜女']
dfItem100_107['一般傷害'] = df_json['一般傷害男']+df_json['一般傷害女']
dfItem100_107['詐欺背信'] = df_json['詐欺背信男']+df_json['詐欺背信女']
dfItem100_107['毒品'] = df_json['毒品男']+df_json['毒品女']
dfItem100_107['公共危險'] = df_json['公共危險男']+df_json['公共危險女']
dfItem100_107['其他'] = df_json['其他男']+df_json['其他女']
dfItem100_107s = pd.melt(dfItem100_107,
                         id_vars="年",
                         value_vars=['竊盜', '一般傷害','詐欺背信','毒品','公共危險','其他'],
                         var_name="案類",
                         value_name="數量")

# ch = (char.Chart(blank_labels=True)
#       .axes.set_yaxis_range(0,20000)
#       .callout.box(top=18047, bottom=11749,left=101, right=102, alpha=0.1, color='green')
#       .callout.text('其他案類在102年較去年的降幅約為34.9%',101.5,16500, font_size='1.2em')
#       .callout.box(top=12167, bottom=8076, left=102, right=106, alpha=0.1, color='red')
#       .callout.text('毒品案類從102年到106年漲幅約為50.65%', 102.5,12000, font_size='1.2em')
#       )
# ch.set_title("各案類總數折線圖")
# ch.set_subtitle('100年到107年')
# ch.set_source_label('Data.Taipei')
# ch.axes.set_xaxis_label('民國年分')
# ch.axes.set_yaxis_label('案類總數')
# ch.plot.line(
#     data_frame=dfItem100_107s,
#     x_column='年',
#     y_column='數量',
#     color_column='案類')
# ch.set_legend_location('outside_bottom')
#
# ch.show()

# 100到107年案件百分比堆疊(年分/案類)_________________________________________________________________________

df_jsonK = df_json.set_index('年', inplace=True)
df_jsonK = df_json.drop(index=[96, 97, 98, 99], columns=['總計男', '總計女']).stack().copy()
df_jsonK = df_jsonK.reset_index()
df_jsonK.columns = ['年', '案類', '數量']
df_jsonK['數量'] = df_jsonK['數量'].astype(int)

# ch = char.Chart(
#     blank_labels=True,
#     y_axis_type='categorical')
#
# ch.plot.bar_stacked(
#     data_frame=df_jsonK,
#     categorical_columns='年',
#     stack_column='案類',
#     numeric_column='數量',
#     normalize=True,
#     categorical_order_by=[100,101,102,103,104,105,106,107],
# )
# ch.set_legend_location('outside_bottom')
# (ch.set_title('100至107年案件百分比堆疊圖')
#  .set_subtitle('年分/案類分計')
#  .axes.set_xaxis_label('各案類占比（％）')
#  .axes.set_yaxis_label('各年度')
#  )
# ch.show()



# ___________________________________________________________________________________________________
# url = "https://quality.data.gov.tw/dq_download_csv.php?nid=125897&md5_url=64fbc8ce005694406e9eb7354bce7099"
# df_origin = pd.read_csv(url, encoding='utf-8')

df_origin = pd.read_csv(r"CriminalByVocation.csv")

df = df_origin.copy()

# 遮罩資料取總計有效值
dfMaskByTotal = (df
                 [(df.Occupation == '總計') & (df.total > 1)
                  & ~df['item'].isin(['重大竊盜', '普通竊盜', '汽車竊盜', '機車竊盜',
                                      '重傷害', '一般傷害', '詐欺', '背信', '故意殺人', '過失致死',
                                      '強制性交', '共同強制性交', '對幼性交', '性交猥褻',
                                      '重大恐嚇取財', '一般恐嚇取財', '第一級毒品', '第二級毒品',
                                      '第三級毒品', '第四級毒品', '強盜', '搶奪'])]
                 .groupby(['year', 'Occupation', 'item'])
                 .last()
                 .sort_values(['year', 'total'], ascending=False)
                 .reset_index()
                 .drop([0, 38])
                 )
dfMaskByTotal.iloc[[0, 37], 2] = '毒品危害'

describe_105_106 = dfMaskByTotal.describe()
print(describe_105_106)

# 分取105年度及106年度
df105 = dfMaskByTotal[dfMaskByTotal.year == '105年']
df106 = dfMaskByTotal[dfMaskByTotal.year == '106年']
# print(df105.corr())
# print(df106.corr())

# 105+106性別與案件種類年度散布圖____________________________________________
# ch = (char.Chart(layout='slid_150%')
#     .axes.set_xaxis_range(0,12000)
#     .axes.set_yaxis_range(0,2000)
#     )
#
#
# df105c = df105.copy()
# df105c.loc[:,'size105'] = df105['total'] ** 0.5 / 2
# df106c = df106.copy()
# df106c.loc[:,'size106'] = df106['total'] ** 0.5 / 2
#
# ch.plot.scatter(data_frame=df105c,
#                 x_column='men',
#                 y_column='Female',
#                 color_column='item',
#                 size_column='size105',
#                 marker='circle')
#
# ch.plot.scatter(data_frame=df106c,
#                 x_column='men',
#                 y_column='Female',
#                 color_column='item',
#                 size_column='size106',
#                 marker='triangle')
# ch.plot.text(
#         data_frame=df105.head(),
#         x_column='men',
#         y_column='Female',
#         text_column='item',
#         color_column=None,
#         x_offset=1,
#         y_offset=-1,
#         font_size='12pt',
#         text_color='ForestGreen'
#         )
#
# ch.plot.text(
#         data_frame=df106.head(),
#         x_column='men',
#         y_column='Female',
#         text_column='item',
#         color_column=None,
#         x_offset=1,
#         y_offset=-1,
#         font_size='12pt',
#         text_color='chocolate',
#         )
#
# ch.callout.text("105年男女相關係數=0.94443\n"
#                 "106年男女相關係數=0.922329\n"
#                 "皆呈高度正相關",0,1500)
#
# (ch.set_title('性別與案件種類年度散布圖')
#  .set_subtitle('○→105年；▲→106年')
#  .set_source_label("Data.Taipei")
#  .axes.set_xaxis_label('男性人數')
#  .axes.set_yaxis_label('女性人數')
#  .set_legend_location(None)
# )
# ch.show()

# 105年前十熱度圖______________________________________________________________________

df105sM = pd.DataFrame(df105, columns=['item', 'men', ]).head(10).copy()
df105sM['genders'] = '男性'
df105sM = df105sM.rename(columns={'men': 'num'})
df105sF = pd.DataFrame(df105, columns=['item', 'Female']).head(10).copy()
df105sF['genders'] = '女性'
df105sF = df105sF.rename(columns={'Female': 'num'})

df105s = pd.concat([df105sF, df105sM])
#
# ch = char.Chart(
#     x_axis_type='categorical',
#     y_axis_type='categorical')
#
# ch.plot.heatmap(
#     data_frame=df105s,
#     y_column='genders',
#     x_column='item',
#     color_column='num',
#     text_column='num',
#     color_palette='Reds',
#     text_format='{0:1,.0f}',
# )
#
# (ch.set_title('105年刑事案件總計')
#  .set_subtitle('前十熱度圖')
#  .set_source_label("Data.Taipei")
#  .axes.set_xaxis_label('案件種類')
#  .axes.set_yaxis_label('性別')
#  .show())

# 106年前十熱度圖______________________________________________________________________

df106sM = pd.DataFrame(df106, columns=['item', 'men', ]).head(10).copy()
df106sM['genders'] = '男性'
df106sM = df106sM.rename(columns={'men': 'num'})
df106sF = pd.DataFrame(df106, columns=['item', 'Female']).head(10).copy()
df106sF['genders'] = '女性'
df106sF = df106sF.rename(columns={'Female': 'num'})

df106s = pd.concat([df106sF, df106sM])

# ch = char.Chart(
#     x_axis_type='categorical',
#     y_axis_type='categorical')
#
# ch.plot.heatmap(
#     data_frame=df106s,
#     y_column='genders',
#     x_column='item',
#     color_column='num',
#     text_column='num',
#     color_palette='Reds',
#     text_format='{:,.0f}')
#
# (ch.set_title('106年刑事案件總計')
#  .set_subtitle('前十熱度圖')
#  .set_source_label("Data.Taipei")
#  .axes.set_xaxis_label('案件種類')
#  .axes.set_yaxis_label('性別')
#  .show())

# 女性案件大於男性長條圖_______________________________________________________________________
checkFmoreM = df[df.Female > df.men]
checkFmoreM2 = (checkFmoreM
                .groupby(['year', 'item'])
                .max()
                .sort_values('Female', ascending=False)
                .reset_index()
                .head(10)
                )

# ch = char.Chart(blank_labels=True,
#                  x_axis_type='categorical')
#
# (ch.set_title("女性案數多於男性之案件類型")
#    .set_subtitle("105年、106年前五")
#    .plot.bar(
#         data_frame=checkFmoreM2,
#         categorical_columns=['year', 'item'],
#         numeric_column='Female',
#         color_column='year'
#     )
# )
# ch.plot.text(
#     data_frame=checkFmoreM2,
#     categorical_columns=['year','item'],
#     numeric_column='Female',
#     text_column='Female',)
# ch.set_legend_location('top_center')
# ch.callout.text('女多於男案類，多見於：」「違反商標法」、「妨害風化」',1.5,50)
# ch.show()

# 105、106年總計(依職業及案類)
dfMaskByOccupation = (df
                      [(~df['Occupation'].isin(['總計'])) & (df.total > 1)
                       & ~df['item'].isin(['重大竊盜', '普通竊盜', '汽車竊盜', '機車竊盜',
                                           '重傷害', '一般傷害', '詐欺', '背信', '故意殺人', '過失致死',
                                           '強制性交', '共同強制性交', '對幼性交', '性交猥褻',
                                           '重大恐嚇取財', '一般恐嚇取財', '第一級毒品', '第二級毒品',
                                           '第三級毒品', '第四級毒品', '強盜', '搶奪'])]
                      .groupby(['year', 'Occupation', 'item'])
                      .last()
                      .sort_values(['year', 'total'], ascending=False)
                      .reset_index()
                      )


dfOccuTotal = dfMaskByOccupation[dfMaskByOccupation.item == '總計']

dfOccu105 = dfOccuTotal[dfOccuTotal.year == '105年']
dfOccu106 = dfOccuTotal[dfOccuTotal.year == '106年']

# 105年個職業案數佔比_________________________________________________________________________________
# plt.figure(figsize=(12, 10))
# font = {'family' : 'Microsoft JhengHei','weight':'bold','size':'8'}
# plt.rc('font', **font)
# plt.rc('axes', unicode_minus=False)
#
# plt.pie(dfOccu105["total"],
#         labels=dfOccu105["Occupation"],
#         autopct="%1.1f%%",
#         explode=(0, 0, 0, 0,0,0,0,0,0,0.1,0.3,0.5,0.8,1.1,1.4),
#         labeldistance=1.05,
#         pctdistance=0.7,
#         textprops={"fontsize":12},
#         startangle=45,
#         shadow=True,
#        )
#
# plt.axis('equal')
# plt.title("105年各職業案數佔比圓餅圖", {"fontsize": 18})
# plt.legend(
#     loc="lower left",
#     bbox_to_anchor = (0.8, 0.,0.3, 1),
#     fontsize=10)
# plt.show()

#106年職業佔比

# plt.figure(figsize=(12, 10))
# font = {'family' : 'Microsoft JhengHei','weight':'bold','size':'8'}
# plt.rc('font', **font)
# plt.rc('axes', unicode_minus=False)
#
# plt.pie(dfOccu106["total"],
#         labels=dfOccu106["Occupation"],
#         autopct="%1.1f%%",
#         explode=(0, 0, 0, 0,0,0,0,0,0,0.1,0.3,0.5,0.8,1.1,1.4),
#         labeldistance=1.05,
#         pctdistance=0.7,
#         textprops={"fontsize":12},
#         startangle=45,
#         shadow=True,
#        )
#
# plt.axis('equal')
# plt.title("106年各職業案數佔比圓餅圖", {"fontsize": 18})
# plt.legend(
#     loc="lower left",
#     bbox_to_anchor = (0.8, 0.,0.3, 1),
#     fontsize=10)
# plt.show()

#105+106前五職業主要案類

dfTop5OccuInTop3Case = (
    dfMaskByOccupation[
        (dfMaskByOccupation['Occupation'].isin(['服務工作者','無職','技藝(技術)有關工作人員','基層技術工及勞力工','學生'])) &
        ~(dfMaskByOccupation['item'] == '總計')]
        .drop(['men','Female'],axis=1)
        .groupby(['Occupation','year'],as_index=True)
        .nth([0,1,2])
        .reset_index()
)
#
# sim1 = checkFmoreM2.iloc[[1,4],[0,1,2,5]]
# sim2 = checkFmoreM2.iloc[[2,3],[0,1,2,5]]
# sim3 = checkFmoreM2.iloc[[6,8],[0,1,2,5]]
# sim4 = checkFmoreM2.iloc[[0],[0,1,2,5]]
# sim5 = checkFmoreM2.iloc[[5],[0,1,2,5]]
# ch = char.Chart(x_axis_type='categorical')
#
# ch.set_title('總案數Top5職業之Top3案類')
# ch.set_subtitle('105年、106年')
# ch.plot.bar(
#     data_frame=dfTop5OccuInTop3Case,
#     categorical_columns=['Occupation', 'year', 'item'],
#     numeric_column='total',
#     color_column='item',)
# ch.plot.text(
#     data_frame=dfTop5OccuInTop3Case,
#     categorical_columns=['Occupation', 'year', 'item'],
#     numeric_column='total',
#     text_column='total',
#     angle=-45)
# ch.set_source_label("Data.Taipei")
# ch.axes.set_xaxis_label('')
# ch.axes.set_yaxis_label('案數')
#
# ch.set_legend_location('outside_bottom')
# ch.show()




