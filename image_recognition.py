#%%
import cv2 as cv

img = cv.imread(r'Lena.jpg')
cv.namedWindow('image')
cv.imshow('image',img)
cv.waitKey()

#%%
import cv2 as cv

img = cv.imread(r'tpe101.jpg')
cv.imshow('image',img)
x = cv.waitKey()
if x ==27:  #27代表的是鍵盤的左上角ESC
    cv.destroyAllWindows()
elif x == ord('s'):
    cv.imwrite(r'tpe101gray.png',img)
#               ↑寫入的檔案名稱    ↑要寫入的檔案
    cv.destroyAllW
    
#%%
import cv2 as cv
import numpy as np

img = np.zeros((512,512,3),np.uint8)     
#a = np.zeros((3,3,3),np.uint0)
#print(a) np.zeros的數組模型測試
#建立512*512像素大小的影像圖，每一個像素中的3個元素都是0(BGR)，由imshow()讀入。
cv.line(img,(0,0),(511,511),(255,0,0),5)
cv.imshow('image',img)
cv.waitKey()

#%%
import cv2 as cv
import numpy as np

n = 300
img = np.zeros((n+1,n+1,3),np.uint8)
img = cv.line(img,(0,0),(n,n),(255,0,0),3)
img = cv.line(img,(0,100),(n,100),(0,255,0),1)
img = cv.line(img,(100,0),(100,n),(0,0,255),6)
cv.imshow('image',img)
cv.waitKey()

#%%
import cv2 as cv
import numpy as np

img = np.zeros((512,512,3),np.uint8)
cv.rectangle(img,(384,0),(510,128),(0,0,255),5)
cv.imshow('image',img)
cv.waitKey()

#%%
import cv2 as cv
import numpy as np

n = 300
img = np.ones((n,n,3),np.uint8)*255
#產生一個300*300的圖形點並包含3個元素像素都是1*255的白色圖片
img = cv.rectangle(img,(50,50),(n-100,n-50),(0,0,255),-1)

cv.imshow('image',img)
cv.waitKey()

#%%
import cv2 as cv
import numpy as np

img = np.zeros((512,512,3),np.uint8)
cv.circle(img,(447,63),63,(0,0,255),-1)
cv.imshow('image',img)
cv.waitKey()

#%%
import cv2 as cv
import numpy as np

img = np.zeros((512,512,3),dtype='uint8')   #512*512像素BGR都是0

for r in range(0,175,25):   #0,25,50,75,100,125,150
    cv.circle(img,(img.shape[1]//2,img.shape[0]//2),r,(255,255,255))
#                   ↑圓心X→512/2      ↑圓心Y→512/2   ↑半徑  
cv.imshow('image',img)
cv.waitKey()

#%%
import cv2 as cv
import numpy as np

d = 400
img = np.ones((d,d,3),np.uint8)*255  #400*400像素的BGR都是1*255

(centerX,centerY) = (round(img.shape[1]/2),round(img.shape[0]/2))  #圓心座標：200,200
#                       ↑用round來處理小數位數到第幾位的四捨五入值，預設參數為整數(不取小數)
red = (0,0,255)
for r in range(5,round(d/2),12):
    cv.circle(img,(centerX,centerY),r,red,3)
cv.imshow('image',img)
cv.waitKey()

#%%
import cv2 as cv
import numpy as np

img = np.zeros((512,512,3),np.uint8)
img.fill(200)

cv.ellipse(img,(180,200),(100,60),45,0,360,(128,0,255),2)
cv.ellipse(img,(180,200),(50,100),45,0,180,(255,0,128),-1)
#cv.ellipse(img,(256,256),(512,40),0,0,360,(128,0,255),2) 測試圖

cv.imshow('image',img)
cv.waitKey()
cv.destroyAllWindows()

#%%
import cv2 as cv
import numpy as np

d = 400
img = np.ones((d,d,3),np.uint8)*255

center = (round(d/2),round(d/2))
size = (100,200)
for i in range(0,10):
    angle = np.random.randint(0,361)
    color = np.random.randint(0,high=256,size=(3,)).tolist()
    thickness = np.random.randint(1,9)
    cv.ellipse(img,center,size,angle,0,360,color,thickness)
cv.imshow('image',img)
cv.waitKey()

#%%
#多邊形圖形(折線圖)
#建立頂點座標
#x = numpy.array([[a,b],[c,d],......],numpy.int32)
#用reshape重新計算調整頂點座標
#x_r = x.reshape(頂點數量,1,2) 頂點數量設為-1,由其他參數計算得出    
#代入polylines函式
#cv2.polylines(img,x_r,isClosed,color,thickness,linetype)

import cv2 as cv
import numpy as np
img = np.zeros((512,512,3),np.uint8)
pts = np.array([[10,5],[60,90],[130,80],[180,100]],np.int32)
pts = pts.reshape((-1,1,2))
cv.polylines(img,[pts],True,(0,255,255))
cv.imshow('image',img)
cv.waitKey()

#%%
import cv2 as cv
import numpy as np
img = np.zeros((512,512,3),np.uint8)
font = cv.FONT_HERSHEY_COMPLEX
cv.putText(img,'OpenCV',(10,350),font,4,(255,255,255),2,cv.LINE_AA)
#                       ↑座標    ↑字型 ↑大小 ↑顏色(白)    ↑反鋸齒處理
cv.imshow('image',img)
cv.waitKey()
cv.destroyAllWindows()

#%%
import cv2 as cv
import numpy as np
img = np.zeros((512,512,3),np.uint8)
img.fill(64)
text = 'OpenCV for Python'

cv.putText(img,text,(10,40),cv.FONT_HERSHEY_SCRIPT_SIMPLEX, \
           1,(0,255,255),1,cv.LINE_AA)
cv.putText(img,text,(10,80),cv.FONT_HERSHEY_PLAIN, \
           1,(0,255,255),2,cv.LINE_AA)
cv.putText(img,text,(10,120),cv.FONT_HERSHEY_DUPLEX, \
           1,(0,255,255),1,cv.LINE_AA)
cv.putText(img,text,(10,160),cv.FONT_HERSHEY_COMPLEX, \
           1,(0,255,255),2,cv.LINE_AA)
cv.putText(img,text,(10,200),cv.FONT_HERSHEY_TRIPLEX, \
           1,(0,255,255),1,cv.LINE_AA)
cv.putText(img,text,(10,240),cv.FONT_HERSHEY_COMPLEX_SMALL, \
           1,(0,255,255),2,cv.LINE_AA)
cv.putText(img,text,(10,280),cv.FONT_HERSHEY_SCRIPT_SIMPLEX, \
           1,(0,255,255),1,cv.LINE_AA)
cv.putText(img,text,(10,320),cv.FONT_HERSHEY_SCRIPT_COMPLEX, \
           1,(0,255,255),2,cv.LINE_AA)
cv.imshow('image',img)
cv.waitKey()
cv.destroyAllWindows()

#%%
import cv2 as cv
import numpy as np

d = 400
img = np.ones((d,d,3),np.uint8)
font = cv.FONT_HERSHEY_SIMPLEX
cv.putText(img,'OpenCV',(0,200),font,3,(0,255,0),10)
cv.putText(img,'OpenCV',(0,200),font,3,(0,0,255),5)
cv.imshow('image',img)
cv.waitKey()

#%%
import cv2 as cv
import numpy as np

d = 400
img = np.ones((d,d,3),np.uint8)*255
font = cv.FONT_HERSHEY_SIMPLEX
cv.putText(img,'OpenCV',(20,150),font,3,(0,0,255),15,cv.LINE_AA)
cv.putText(img,'OpenCV',(20,220),font,3,(0,255,0),15, \
           cv.FONT_HERSHEY_SCRIPT_SIMPLEX,True)
cv.imshow('image',img)
cv.waitKey()

#%%
from PIL import ImageFont, ImageDraw, Image
import cv2 as cv
import numpy as np

img = np.zeros((512,512,3),np.uint8)
img.fill(64)

img[:] = (0,0,192)
text = '恭賀\n新囍'

fontPath = r'C:\Users\ASUS\Desktop\109100112\Python\ImageRecognition\TaipeiSansTCBeta-Light.ttf'

font = ImageFont.truetype(fontPath,224) #載入字體

imgPil = Image.fromarray(img)   #將numpy中的陣列轉成pil圖像

draw = ImageDraw.Draw(imgPil)   #在pil上寫文字
draw.text((30,30),text,font=font,fill=(0,0,0,0)) #使用text方法將位置、字型及大小寫入文字

img = np.array(imgPil)
cv.imshow('image',img)
cv.waitKey()
cv.destroyAllWindows()